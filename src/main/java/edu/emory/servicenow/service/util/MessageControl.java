package edu.emory.servicenow.service.util;


public enum MessageControl {

	CREATE_MSG_ACTION("Create"), 
	DATA_AREA("DataArea"),
	DELETE_DATA("DeleteData"),	
	DELETE_MSG_ACTION("Delete"), 
	GENERATE_MSG_ACTION("Generate"),
	NEW_DATA("NewData"),
	BASELINE_DATA("BaselineData"),
	PROVIDE_MSG_ACTION("Provide"),
	QUERY_MSG_ACTION("Query"), 
	REQUEST_MSG_TYPE("Request"), 
	REPLY_MSG_TYPE("Reply"), 	
	FIREWALL_EXCEPTION_ADD_REQUEST("FirewallExceptionAddRequest"),	
	FIREWALL_EXCEPTION_ADD_REQUEST_QUERY_SPECIFICATION("FirewallExceptionAddRequestQuerySpecification"),
	FIREWALL_EXCEPTION_ADD_REQUEST_REQUISITION("FirewallExceptionAddRequestRequisition"),
	FIREWALL_EXCEPTION_REMOVE_REQUEST("FirewallExceptionRemoveRequest"),	
	FIREWALL_EXCEPTION_REMOVE_REQUEST_QUERY_SPECIFICATION("FirewallExceptionRemoveRequestQuerySpecification"),
	FIREWALL_EXCEPTION_REMOVE_REQUEST_REQUISITION("FirewallExceptionRemoveRequestRequisition"),
	INCIDENT_QUERY_SPECIFICATION("IncidentQuerySpecification"),	
	INCIDENT("Incident"),
	INCIDENT_REQUISITION("IncidentRequisition"),
	INCIDENT_OBJECT("Incident"),
	ELASTICIP_REQUEST_QUERY_SPECIFICATION("ElasticIpRequestQuerySpecification"),
	//ELASTICIP_REQUEST("ElasticIpRequestRequisition"),
	ELASTICIP_REQUEST("ElasticIpRequest"),		
	UPDATE_MSG_ACTION("Update"),
	XML_OUTPUT("xml");
		
	private final String name;
	public String getName() {
		return name;
	}
	private MessageControl(String s) {
		this.name = s;
	}
	public String toString() {
		return name;
	}
}
