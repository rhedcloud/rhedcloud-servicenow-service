package edu.emory.servicenow.service.util;

public enum ServiceNowValues {
	ESB_CREATED_REQUEST("ESB Created Request"),
	FIREWALL_REQUEST("Firewall"),
	ADD_EXCEPTION("Add Exception"),
	REMOVE_EXCEPTION("Remove Exception"),
	OPEN_STATE("Open"),
	TASK_INCOMPLETE_STATE("Task Incomplete"),
	CLOSED_CANCEL_STATE("Closed Cancelled");

	private final String name;
	public String getName() {
		return name;
	}
	private ServiceNowValues(String s) {
		this.name = s;
	}
	public String toString() {
		return name;
	}
}
