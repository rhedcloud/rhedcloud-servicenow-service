
package edu.emory.servicenow.service.util;

public enum PropertyNames {
		
	CONNECTION_TIMEOUT("connectionTimeout"),	
	GENERAL_PROPERTIES("GeneralProperties"), 
	FIREWALL_WEB_SERVICE_ENDPOINT("firewallWebServiceEndpoint"),
	ELASTICIP_WEB_SERVICE_ENDPOINT("elasticIpWebServiceEndpoint"),
	INCIDENT_WEB_SERVICE_ENDPOINT("incidentWebServiceEndpoint"),
	SERVICE_PROVIDER_CLASSNAME("servicenowServiceProviderClassName"),
	SOCKET_TIMEOUT("socketTimeout"),
	WEB_SERVICE_NAME("webServiceName"), 
	WAIT_INTERVAL("waitInterval"), 
	MAX_WAIT_TIME("maxWaitTime"), 
	WEB_SERVICE_USER_ID("webServiceUserId"), 
	WEB_SERVICE_PASSWORD("webServicePassword"); 
		
	private final String name; 
		public String getName() {
			return name;
		}
		private PropertyNames(String s){this.name = s;}
		public String toString() {return name;}
	}


