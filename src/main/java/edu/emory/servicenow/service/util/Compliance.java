package edu.emory.servicenow.service.util;

public enum Compliance {
		HIPAA("HIPAA"),
		FERPA("FERPA"),
		FISMA("FISMA"),		
		UNSURE("UNSURE"),
		OTHER("OTHER"),
		PCI("PCI");
		
	private final String name;
	public String getName() {
		return name;
	}
	private Compliance(String s) {
		this.name = s;
	}
	public String toString() {
		return name;
	}
}
