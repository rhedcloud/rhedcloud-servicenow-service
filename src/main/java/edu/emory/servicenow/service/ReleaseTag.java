/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory Redcap Connector Service.

 Copyright (C) 2016 Emory University. All rights reserved.

 This service was developed by Emory University for use by Emory and the 
 Atlanta Clinical & Translational Science Institute. 
 ******************************************************************************/

package edu.emory.servicenow.service;

public abstract class ReleaseTag {

  public static String space = " ";
  public static String notice = "***";
  public static String releaseName = "Servicenow Service";
  public static String releaseNumber = "Release 1.0";
  public static String buildNumber = "Build ####";
  public static String copyRight = "Copyright 2016 Emory University." +
    " All Rights Reserved.";
	
  public static String getReleaseInfo() {
    StringBuffer sbuf = new StringBuffer();
    sbuf.append(notice);
    sbuf.append(space);
    sbuf.append(releaseName);
    sbuf.append(space);
    sbuf.append(releaseNumber);
    sbuf.append(space);
    sbuf.append(buildNumber);
    sbuf.append(space);
    sbuf.append(copyRight);
    sbuf.append(space);
    sbuf.append(notice);
    return sbuf.toString();
  }
}
