package edu.emory.servicenow.service;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;
import org.openeai.OpenEaiException;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.jms.consumer.commands.RequestCommandImpl;
import org.openeai.jms.producer.MessageProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.moa.EnterpriseObjectSyncException;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.moa.XmlEnterpriseObjectImpl;
import org.openeai.moa.objects.resources.Authentication;
import org.openeai.transport.SyncService;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;

import com.service_now.moa.jmsobjects.customrequests.v1_0.ElasticIpRequest;
import com.service_now.moa.jmsobjects.customrequests.v1_0.FirewallExceptionAddRequest;
import com.service_now.moa.jmsobjects.customrequests.v1_0.FirewallExceptionRemoveRequest;
import com.service_now.moa.jmsobjects.servicedesk.v2_0.Incident;
import com.service_now.moa.objects.resources.v1_0.ElasticIpRequestQuerySpecification;
import com.service_now.moa.objects.resources.v1_0.FirewallExceptionAddRequestQuerySpecification;
import com.service_now.moa.objects.resources.v1_0.FirewallExceptionAddRequestRequisition;
import com.service_now.moa.objects.resources.v1_0.FirewallExceptionRemoveRequestQuerySpecification;
import com.service_now.moa.objects.resources.v1_0.FirewallExceptionRemoveRequestRequisition;
import com.service_now.moa.objects.resources.v2_0.IncidentQuerySpecification;
import com.service_now.moa.objects.resources.v2_0.IncidentRequisition;

import edu.emory.servicenow.service.provider.ElasticIpRequestProvider;
import edu.emory.servicenow.service.provider.FirewallExceptionAddRequestProvider;
import edu.emory.servicenow.service.provider.FirewallExceptionRemoveRequestProvider;
import edu.emory.servicenow.service.provider.IncidentProvider;
import edu.emory.servicenow.service.provider.ProviderException;
import edu.emory.servicenow.service.provider.ServicenowElasticIpRequestProvider;
import edu.emory.servicenow.service.provider.ServicenowFirewallExceptionAddRequestProvider;
import edu.emory.servicenow.service.provider.ServicenowFirewallExceptionRemoveRequestProvider;
import edu.emory.servicenow.service.provider.ServicenowIncidentProvider;
import edu.emory.servicenow.service.util.MessageControl;
import edu.emory.servicenow.service.util.PropertyNames;

/**
 * This request command handles Request messages for ServiceNow
 * FirewallException, ElasticIP and Incident message objects. Specifically, it
 * handles a Query-Request, a Generate-Request, a Update-Request and a
 * Delete-Request for the objects.
 * 
 * @author Richard Xing (RXING2)
 * @author nchen3
 * 
 * @version 1.0 - 11 February 2018
 * @version 1.1 - September 2018
 */
public class ServicenowServiceRequestCommand extends RequestCommandImpl implements RequestCommand {
	private static String serviceUserId = "";
	private static String servicePassword = "";
	private static String searchServiceEndpoint = "";
	private static int connectionTimeout = 12000;
	private ProducerPool producerPool = null;
	private static int socketTimeout = 12000;
	private Document responseDoc = null;
	private FirewallExceptionAddRequestProvider firewallExceptionAddProvider = null;
	private FirewallExceptionRemoveRequestProvider firewallExceptionRemoveProvider = null;
	private IncidentProvider incidentProvider = null;
	private ElasticIpRequestProvider elasticIPProvider = null;
	private static final String LOGTAG = "[" + ServicenowServiceRequestCommand.class.getSimpleName() + "]";;

	public ServicenowServiceRequestCommand(CommandConfig cConfig) throws InstantiationException, ProviderException {
		super(cConfig);

		logger.info(LOGTAG + "ServicenowServiceRequestCommand is initializing...");
		try {
			ProducerPool pool = (ProducerPool) getAppConfig().getObject("SyncPublisher");
			setProducerPool(pool);
		} catch (EnterpriseConfigurationObjectException eoce) {
			String errMsg = "Error retrieving a ProducerPool object " + "from AppConfig. The exception is: "
					+ eoce.getMessage();
			logger.fatal(LOGTAG + errMsg);
			throw new InstantiationException(errMsg);
		}

		try {
			PropertyConfig pConfig = (PropertyConfig) getAppConfig().getObject("GeneralProperties");
			setProperties(pConfig.getProperties());
		} catch (EnterpriseConfigurationObjectException ecoe) {
			String errMsg = "An error occurred retrieving a property config from AppConfig. The exception is: "
					+ ecoe.getMessage();
			logger.fatal(LOGTAG + errMsg);
			throw new InstantiationException(errMsg);
		}

		// Initialize response document.
		XmlDocumentReader xmlReader = new XmlDocumentReader();
		try {
			logger.debug(LOGTAG + "responseDocumentUri: " + getProperties().getProperty("responseDocumentUri"));
			responseDoc = xmlReader.initializeDocument(getProperties().getProperty("responseDocumentUri"),
					getOutboundXmlValidation());
			if (responseDoc == null) {
				String errMsg = LOGTAG + "Missing 'responseDocumentUri' "
						+ "property in the deployment descriptor.  Can't continue.";
				logger.fatal(LOGTAG + errMsg);
				throw new InstantiationException(errMsg);
			}
			setResponseDoc(responseDoc);
		} catch (XmlDocumentReaderException e) {
			String errMsg = "Error initializing the primed documents.";
			logger.fatal(LOGTAG + errMsg);
			throw new InstantiationException(e.getMessage());
		}

		String className = getProperties().getProperty(PropertyNames.SERVICE_PROVIDER_CLASSNAME.getName());
		logger.info(LOGTAG + "Getting class for name: " + className);

		if (className == null || className.equals("")) {
			String errMsg = "No ServiceNowRequestProviderClassName property specified. Can't continue. Please define provider class name from AppConfig.";
			logger.fatal(LOGTAG + errMsg);
			throw new InstantiationException(errMsg);
		}

		logger.info(LOGTAG + "ServicenowServiceProviderClassName is: " + className);
		
		this.firewallExceptionAddProvider = new ServicenowFirewallExceptionAddRequestProvider();
		try {
			this.firewallExceptionAddProvider.init(getAppConfig());
		} catch (ProviderException pe) {
			String errMsg = "Error initializing the ServicenowFirewallExceptionAddProvider object from AppConfig: The exception is: "
					+ pe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new InstantiationException(errMsg);
		}
		
		this.firewallExceptionRemoveProvider = new ServicenowFirewallExceptionRemoveRequestProvider();
		try {
			this.firewallExceptionRemoveProvider.init(getAppConfig());
		} catch (ProviderException pe) {
			String errMsg = "Error initializing the ServicenowFirewallExceptionRemoveProvider object from AppConfig: The exception is: "
					+ pe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new InstantiationException(errMsg);
		}
		

		this.elasticIPProvider = new ServicenowElasticIpRequestProvider();
		try {
			this.elasticIPProvider.init(getAppConfig());
			setElasticIPProvider(elasticIPProvider);
		} catch (ProviderException pe) {
			String errMsg = "Error initializing the ServicenowElasticIPProvider object from AppConfig: The exceptionis2: "
					+ pe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new InstantiationException(errMsg);
		}

		this.incidentProvider = new ServicenowIncidentProvider();
		try {
			this.incidentProvider.init(getAppConfig());
			setIncidentProvider(incidentProvider);
		} catch (ProviderException pe) {
			String errMsg = "Error initializing the ServicenowIncidentProvider object from AppConfig: The exceptionis3: "
					+ pe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new InstantiationException(errMsg);
		}
	}

	public FirewallExceptionAddRequestProvider getFirewallExceptionAddProvider() {
		return firewallExceptionAddProvider;
	}

	public void setFirewallExceptionAddProvider(FirewallExceptionAddRequestProvider firewallExceptionAddProvider) {
		this.firewallExceptionAddProvider = firewallExceptionAddProvider;
	}

	public FirewallExceptionRemoveRequestProvider getFirewallExceptionRemoveProvider() {
		return firewallExceptionRemoveProvider;
	}

	public void setFirewallExceptionRemoveProvider(FirewallExceptionRemoveRequestProvider firewallExceptionRemoveProvider) {
		this.firewallExceptionRemoveProvider = firewallExceptionRemoveProvider;
	}

	@Override
	public final Message execute(int messageNumber, Message aMessage) throws CommandException {

		logger.info(LOGTAG + "In execute() - messageNumber: "+messageNumber);
		Document requestFromEai = new Document();
		TextMessage msg = (TextMessage) aMessage;
		try {
			requestFromEai = initializeInput(messageNumber, aMessage);
			msg.clearBody();
		} catch (JMSException je) {
			String errMsg = "Exception occurred processing input message in "
					+ "org.openeai.jms.consumer.commands.Command.  Exception is: " + je.getMessage();
			throw new CommandException(errMsg);
		}
		
		// Get Control area from XML object
		Element eControlArea = getControlArea(requestFromEai.getRootElement());
		String msgAction = this.getMessageAction(requestFromEai);
		String msgObject = this.getMessageObject(requestFromEai);
		String msgRelease = this.getMessageRelease(requestFromEai);
		Element eAuthUserId = eControlArea.getChild("Sender").getChild("Authentication").getChild("AuthUserId");
		String authUserId = eAuthUserId.getValue();

		// parse the text from the JMS message into a JDOM document.
		logger.info(LOGTAG + "msgAction: " + this.getMessageAction(requestFromEai));
		logger.info(LOGTAG + "msgType: " + this.getMessageType(requestFromEai));
		logger.info(LOGTAG + "msgObject: " + this.getMessageObject(requestFromEai));
		logger.info(LOGTAG + "msgRelease: " + this.getMessageRelease(requestFromEai));
		
		try {

			Document responseXmlObject = (Document) getResponseXml(msgAction, msgObject, msgRelease).clone();

			if (MessageControl.FIREWALL_EXCEPTION_ADD_REQUEST.toString().equalsIgnoreCase(msgObject)) {

				return processFirewallExceptionAddRequest(msg, requestFromEai, responseXmlObject);

			} else if (MessageControl.FIREWALL_EXCEPTION_REMOVE_REQUEST.toString().equalsIgnoreCase(msgObject)) {

				return processFirewallExceptionRemoveRequest(msg, requestFromEai, responseXmlObject);

			} else if (MessageControl.ELASTICIP_REQUEST.toString().equalsIgnoreCase(msgObject)) {

				ElasticIpRequest elasticIP = new ElasticIpRequest();
				try {
					elasticIP = this.getObject(ElasticIpRequest.class);
				} catch (EnterpriseConfigurationObjectException ecoe) {
					String errMsg = "Error retrieving an object from AppConfig. The exception is: " + ecoe.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new CommandException(errMsg, ecoe);
				}

				Element eElasticIP = null;
				if (MessageControl.CREATE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {

					eElasticIP = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
							.getChild(MessageControl.NEW_DATA.toString())
							.getChild(MessageControl.ELASTICIP_REQUEST.toString());

					if (eElasticIP == null) {
						String errMsg = "Invalid element found in the Create-Request message. This command expects a ElasticIP object";
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg,
								"error");
					}

					try {
						elasticIP.buildObjectFromInput(eElasticIP);
					} catch (EnterpriseLayoutException ele) {
						String errMsg = "An error occurred building the Create object from the DataArea element in the Create-Request message. The exception is: "
								+ ele.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "ServiceNow-1000", eControlArea, msgRelease, msg, "error");
					}

					try {
						// getElasticIPProvider().generate(elasticIPRequisition);
						getElasticIPProvider().create(elasticIP);
					} catch (ProviderException pe) {
						String errMsg = "An error occurred generating FirewallException in Generate-Request. The exception is: "
								+ pe.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg,
								"error");
					}

					if (elasticIP != null) {
						try {
							MessageProducer producer = getProducerPool().getProducer();
							logger.info(LOGTAG + "Publishing ElasticIP object Create-Sync");
							Authentication auth = new Authentication();
							auth.setAuthUserId(authUserId);
							auth.setAuthUserSignature("none");
							elasticIP.setAuthentication(auth);
							elasticIP.createSync((SyncService) producer);
							logger.info(LOGTAG + "Published ElasticIpRequest.Create-Sync" + " message.");
						} catch (EnterpriseObjectSyncException eose) {
							String errMsg = "An error occurred publishing the ElasticIP.Create-Sync message after generating Resource.";
							logger.error(LOGTAG + errMsg);
							throw new CommandException(eose);
						} catch (JMSException jmse) {
							String errMsg = "An error occurred publishing the ElasticIP.Create-Sync message after generating Resource.";
							logger.error(LOGTAG + errMsg);
							throw new CommandException(jmse);
						}
					}
					logger.info(LOGTAG + "In execute(). Completed returning oEAI object and publishing Create-Sync.");

					responseXmlObject.getRootElement().getChild(MessageControl.DATA_AREA.getName()).removeContent();
					logger.info(LOGTAG + "In execute(). Completed returning oEAI object and publishing Create-Sync.");
					String replyContents = buildReplyDocument(eControlArea, responseXmlObject);

					return getMessage(msg, replyContents);
				} else if (MessageControl.QUERY_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {

					eElasticIP = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
							.getChild(MessageControl.ELASTICIP_REQUEST_QUERY_SPECIFICATION.toString());
					XMLOutputter outputter = new XMLOutputter();

					List<XmlEnterpriseObject> elasticIpObjects = null;
					logger.info(LOGTAG + "Query messages: " + outputter.outputString(eElasticIP));
					ElasticIpRequestQuerySpecification queryData = new ElasticIpRequestQuerySpecification();

					if (eElasticIP != null) {
						try {
							queryData = this.getObject(ElasticIpRequestQuerySpecification.class);

							if (queryData != null) {
								queryData.buildObjectFromInput(eElasticIP);
							}
						} catch (EnterpriseLayoutException ele) {
							String errMsg = "An error occurred serializing ElasticIP object to an XML element. The exception is: "
									+ ele.getMessage();
							logger.error(LOGTAG + errMsg);
							throw new CommandException(errMsg, ele);
						} catch (EnterpriseConfigurationObjectException e) {
							String errMsg = "Error retrieving an object from AppConfig: " + "The exception" + "is: "
									+ e.getMessage();
							logger.error(LOGTAG + errMsg);
							throw new CommandException(errMsg);
						}
						// logger.info(LOGTAG + "queryData is build: " + queryData.getAppName());
						try {
							elasticIpObjects = getElasticIPProvider().query(queryData);
						} catch (ProviderException pe) {
							String errMsg = "An error occurred generating Query Elastic IP object in Query-Request. The exception is: "
									+ pe.getMessage();
							OpenEaiException e = new OpenEaiException(errMsg + msgObject);
							return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg,
									"error");
						}
					}

					// start
					// Send resonse
					Element elasticIpElement = null;
					List<Element> elementList = new ArrayList<Element>();
					Element responseDataArea = responseXmlObject.getRootElement().getChild(DATA_AREA);
					responseDataArea.removeContent();

					if (elasticIpObjects != null && elasticIpObjects.size() > 0) {
						for (XmlEnterpriseObject eObj : elasticIpObjects) {
							try {
								elasticIpElement = getElementFromEnterpriseObject(eObj);
							} catch (OpenEaiException oee) {
								String errMsg = "An error occurred converting an OpenEAI object. The exception is: "
										+ oee.getMessage();
								logger.error(LOGTAG + errMsg);
								throw new CommandException(errMsg, oee);
							}
							elementList.add(elasticIpElement);
						}
						if (!elementList.isEmpty()) {
							logger.info(LOGTAG + "In excute(). Setting data area elements. Elements to set: "
									+ elementList.size());
							for (Element element : elementList) {
								responseDataArea.addContent(element);
							}
						}
					} else {
						logger.info(LOGTAG
								+ "The object returned from Query-Request is empty. The record does not exist on ServiceNow. ");
					}
					String responseMessage = buildReplyDocument(eControlArea, responseXmlObject);
					Message responseToEAI = getMessage(msg, responseMessage);
					return responseToEAI;

				} else if (MessageControl.UPDATE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {
					Element eBaselineData = requestFromEai.getRootElement()
							.getChild(MessageControl.DATA_AREA.toString())
							.getChild(MessageControl.BASELINE_DATA.getName())
							.getChild(MessageControl.ELASTICIP_REQUEST.getName());

					Element eNewData = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
							.getChild(MessageControl.NEW_DATA.getName())
							.getChild(MessageControl.ELASTICIP_REQUEST.getName());

					if (eNewData == null || eBaselineData == null) {
						String errMsg = "Either the baseline or new data state of the ElasticIP is null. Can't continue.";
						throw new CommandException(errMsg);
					}

					ElasticIpRequest baselineElasticIP = new ElasticIpRequest();
					ElasticIpRequest servicenowElasticIP = new ElasticIpRequest();
					try {
						baselineElasticIP = (ElasticIpRequest) getAppConfig()
								.getObjectByType(baselineElasticIP.getClass().getName());
						elasticIP = (ElasticIpRequest) getAppConfig().getObjectByType(elasticIP.getClass().getName());
					} catch (EnterpriseConfigurationObjectException ecoe) {
						String errMsg = "An error occurred retrieving an object from " + "AppConfig. The exception is: "
								+ ecoe.getMessage();
						logger.error(LOGTAG + errMsg);
						throw new CommandException(errMsg, ecoe);
					}

					try {
						baselineElasticIP.buildObjectFromInput(eBaselineData);
						elasticIP.buildObjectFromInput(eNewData);
					} catch (EnterpriseLayoutException ele) {
						String errMsg = "An error occurred building the baseline and newdata states of the ElasticIP object passed in. The exception is: "
								+ ele.getMessage();
						throw new CommandException(errMsg, ele);
					}

					// Perform the baseline check.
					try {

						servicenowElasticIP = getElasticIPProvider().query(elasticIP);
						if (servicenowElasticIP == null) {
							String errDesc = "Baseline is stale and no identity from ServiceNow matches the baseline. No update operation may be performed.";
							logger.warn(LOGTAG + errDesc);
							OpenEaiException e = new OpenEaiException(errDesc + msgObject);
							return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg,
									"warn");
						}

						if (baselineElasticIP.equals(servicenowElasticIP)) {
							String errDesc = "Baseline matches the current state of the ElasticIP in the ServiceNow service.";
							logger.info(LOGTAG + errDesc);
						} else {
							String errDesc = "Baseline does not matches the current state of the ElasticIP in the ServiceNow service.";
							logger.info(LOGTAG + errDesc);
						}

						// Verify that the baseline and the new state are not equal.
						if (baselineElasticIP.equals(elasticIP)) {
							String errDesc = "Baseline state and new state of the identity are equal. No update operation may be performed.";
							logger.warn(LOGTAG + errDesc);
							OpenEaiException e = new OpenEaiException(errDesc + msgObject);
							return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg,
									"warn");
						}
					} catch (XmlEnterpriseObjectException xeoe) {
						String errMsg = "An error occurred comparing the baseline and new data. The exception is: "
								+ xeoe.getMessage();
						logger.error(LOGTAG + errMsg);
						throw new CommandException(errMsg, xeoe);
					} catch (ProviderException pe) {
						String errMsg = "An error occurred Querying in Update-Request. The exception is: "
								+ pe.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg, "warn");
					}

					try {
						getElasticIPProvider().update(elasticIP);
					} catch (ProviderException pe) {
						String errMsg = "An error occurred Querying in Update-Request. The exception is: "
								+ pe.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg, "warn");
					}

					try {
						MessageProducer producer = getProducerPool().getProducer();
						try {
							elasticIP.setBaseline(baselineElasticIP);
							logger.info(LOGTAG + elasticIP.toXmlString());
						} catch (XmlEnterpriseObjectException xeoe) {
							String errMsg = "An error occurred comparing the baseline and new data. The exception is: "
									+ xeoe.getMessage();
							logger.error(LOGTAG + errMsg);
							throw new CommandException(errMsg, xeoe);
						}
						elasticIP.updateSync((SyncService) producer);
					} catch (EnterpriseObjectSyncException eose) {
						String errMsg = "An error occurred publishing the FirewallExceptionRequest.Update-Sync message after updating FirewallException. The exception is: "
								+ eose.getMessage();
						logger.error(LOGTAG + errMsg);
						throw new CommandException(errMsg, eose);
					} catch (JMSException jmse) {
						String errMsg = "An error occurred publishing the FirewallExceptionRequest.Update-Sync message after updating ElasticIP. The exception is: "
								+ jmse.getMessage();
						logger.error(LOGTAG + errMsg);
						throw new CommandException(errMsg, jmse);
					}
				} else if (MessageControl.DELETE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {
					eElasticIP = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
							.getChild(MessageControl.DELETE_DATA.toString())
							.getChild(MessageControl.ELASTICIP_REQUEST.toString());

					if (eElasticIP == null) {
						String errMsg = "Invalid element found in the Delete-Request message. This command expects a ElasticIP object";
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, errMsg, eControlArea, msgRelease, msg, "error");
					}

					try {
						elasticIP.buildObjectFromInput(eElasticIP);
					} catch (EnterpriseLayoutException ele) {
						String errMsg = "An error occurred building the delete object from the DataArea element in the Delete-Request message. The exception is: "
								+ ele.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "ServiceNowService-1000", eControlArea, msgRelease, msg,
								"error");
					}
					// logger.info(LOGTAG + "queryData is build: " + eElasticIP.getText());

					try {
						getElasticIPProvider().delete(elasticIP.getSystemId());
					} catch (ProviderException pe) {
						String errMsg = "An error occurred deleting ElasticIP in Delete-Request message. The exception is: "
								+ pe.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "ServiceNowService-1000", eControlArea, msgRelease, msg,
								"error");
					}

					try {
						MessageProducer producer = getProducerPool().getProducer();
						logger.info(LOGTAG + "Publishing Resource Delete-Sync");
						elasticIP.deleteSync("purge", (SyncService) producer);
						logger.info(LOGTAG + "Published Resource.Delete-Sync" + " message.");
					} catch (EnterpriseObjectSyncException eose) {
						String errMsg = "An error occurred publishing the Resource.Delete-Sync message after deleting Resource.";
						logger.error(LOGTAG + errMsg);
						throw new CommandException(eose);
					} catch (JMSException jmse) {
						String errMsg = "An error occurred publishing the Resource.Delete-Sync message after deleting Resource.";
						logger.error(LOGTAG + errMsg);
						throw new CommandException(jmse);
					}
				} else {

					String errMsg = "The ServiceNow Service doesn't support the input message action. Verify that the sending application is sending appropriate message.";
					OpenEaiException e = new OpenEaiException(errMsg + msgAction);
					return getExceptionResponse(e, errMsg, eControlArea, msgRelease, msg, "error");
				}

				responseXmlObject.getRootElement().getChild(MessageControl.DATA_AREA.getName()).removeContent();
				String replyContents = buildReplyDocument(eControlArea, responseXmlObject);
				return getMessage(msg, replyContents);

			} else if (MessageControl.INCIDENT_OBJECT.toString().equalsIgnoreCase(msgObject)) {

				Incident incident = new Incident();
				try {
					incident = this.getObject(Incident.class);
				} catch (EnterpriseConfigurationObjectException ecoe) {
					String errMsg = "Error retrieving an object from AppConfig. The exception is: " + ecoe.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new CommandException(errMsg, ecoe);
				}

				Element eIncident = null;
				// if (MessageControl.CREATE_MSG_ACTION.toString().equalsIgnoreCase(msgAction))
				// {
				// // Element eIncident = null;
				// eIncident =
				// requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
				// .getChild(MessageControl.NEW_DATA.toString()).getChild(MessageControl.INCIDENT.toString());
				// if (eIncident == null) {
				// String errMsg = "Invalid element found in the Create-Request message. This
				// command expects a Incident object";
				// OpenEaiException e = new OpenEaiException(errMsg + msgObject);
				// return getExceptionResponse(e, "ServiceNowService-100X", eControlArea,
				// msgRelease, msg, "error");
				// }
				//
				// try {
				// if (incident != null) {
				// incident.buildObjectFromInput(eIncident);
				// }
				// try {
				// getIncidentProvider().create(incident);
				// } catch (ProviderException pe) {
				// String errMsg = "An error occurred creating Incident in Create-Request. The
				// exception is: "
				// + pe.getMessage();
				// OpenEaiException e = new OpenEaiException(errMsg + msgObject);
				// return getExceptionResponse(e, "ServiceNowService-100X", eControlArea,
				// msgRelease, msg,
				// "error");
				// }

				if (MessageControl.GENERATE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {
					Element eIncidentRequisition = null;
					eIncidentRequisition = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
							.getChild(MessageControl.INCIDENT_REQUISITION.toString());
					if (eIncidentRequisition == null) {
						String errMsg = "Invalid element found in the Generate-Request message. This command expects a IncidentRequisition object";
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg,
								"error");
					}

					XMLOutputter outputter = new XMLOutputter();
					logger.info("Generate message: " + outputter.outputString(eIncidentRequisition));

					IncidentRequisition incidentRequisition = new IncidentRequisition();
					try {
						incidentRequisition = (IncidentRequisition) getAppConfig()
								.getObject("IncidentRequisition" + "." + generateRelease(msgRelease));
						if (incidentRequisition != null) {
							incidentRequisition.buildObjectFromInput(eIncidentRequisition);
						}
						try {
							incident = getIncidentProvider().generate(incidentRequisition);
						} catch (ProviderException pe) {
							String errMsg = "An error occurred generating Incident in Generate-Request. The exception is: "
									+ pe.getMessage();
							OpenEaiException e = new OpenEaiException(errMsg + msgObject);
							return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg,
									"error");
						}
						// Issue a create sync only on Generate-Request
						if (incident != null) {
							try {
								MessageProducer producer = getProducerPool().getProducer();
								logger.info(LOGTAG + "Publishing Incident Create-Sync");
								Authentication auth = new Authentication();
								auth.setAuthUserId(authUserId);
								auth.setAuthUserSignature("none");
								incident.setAuthentication(auth);
								incident.createSync((SyncService) producer);
								logger.info(LOGTAG + "Published .Create-Sync" + " message.");
							} catch (EnterpriseObjectSyncException eose) {
								String errMsg = "An error occurred publishing the Incident.Create-Sync message after generating an identity.";
								logger.error(LOGTAG + errMsg);
								throw new CommandException(eose);
							} catch (JMSException jmse) {
								String errMsg = "An error occurred publishing the Incident.Create-Sync message after generating an identity.";
								logger.error(LOGTAG + errMsg);
								throw new CommandException(jmse);
							}
						}
						logger.info(
								LOGTAG + "In execute(). Completed returning oEAI object and publishing Create-Sync.");
					} catch (EnterpriseLayoutException ele) {
						String errMsg = "An error occurred serializing Incident object to an XML element. The exception is: "
								+ ele.getMessage();
						logger.error(LOGTAG + errMsg);
						throw new CommandException(errMsg, ele);
					} catch (EnterpriseConfigurationObjectException ecoe) {
						String errMsg = "An error occurred retrieving an object from " + "AppConfig. The exception is: "
								+ ecoe.getMessage();
						logger.error(LOGTAG + errMsg);
						throw new CommandException(errMsg, ecoe);
					}

					logger.info(LOGTAG + "In execute(). Completed returning oEAI object and publishing Create-Sync.");

					responseXmlObject.getRootElement().getChild(MessageControl.DATA_AREA.getName()).removeContent();
					logger.info(LOGTAG + "In execute(). Completed returning oEAI object and publishing Create-Sync.");
					// String replyContents = buildReplyDocument(eControlArea, responseXmlObject);
					//
					// return getMessage(msg, replyContents);
					Element elementIncident = null;
					try {
						elementIncident = (Element) incident.buildOutputFromObject();
					} catch (EnterpriseLayoutException ele) {
						String errMsg = "An error occurred serializing a Incident object to an XML element. The exception is: "
								+ ele.getMessage();
						logger.error(LOGTAG + errMsg, ele);
						throw new CommandException(errMsg, ele);
					}
					responseXmlObject.getRootElement().getChild(MessageControl.DATA_AREA.getName())
							.addContent(elementIncident);
					String replyContents = buildReplyDocument(eControlArea, responseXmlObject);
					return getMessage(msg, replyContents);

				} else if (MessageControl.QUERY_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {

					Element eQuerySpecification = null;
					eQuerySpecification = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
							.getChild(MessageControl.INCIDENT_QUERY_SPECIFICATION.toString());

					List<XmlEnterpriseObject> incidentObjects = null;
					XMLOutputter outputter = new XMLOutputter();
					logger.info("Query messages: " + outputter.outputString(eQuerySpecification));
					IncidentQuerySpecification queryData = new IncidentQuerySpecification();

					if (eQuerySpecification != null) {
						try {
							queryData = this.getObject(IncidentQuerySpecification.class);
							if (queryData != null) {
								queryData.buildObjectFromInput(eQuerySpecification);
							}
						} catch (EnterpriseLayoutException ele) {
							String errMsg = "An error occurred serializing Incident object to an XML element. The exception is: "
									+ ele.getMessage();
							logger.error(LOGTAG + errMsg);
							throw new CommandException(errMsg, ele);
						} catch (EnterpriseConfigurationObjectException e) {
							String errMsg = "Error retrieving an object from AppConfig: " + "The exception" + "is: "
									+ e.getMessage();
							logger.error(LOGTAG + errMsg);
							throw new CommandException(errMsg);
						}
						// logger.info(LOGTAG + "queryData is build: " + queryData.getNumber());
						try {
							incidentObjects = getIncidentProvider().query(queryData);
						} catch (ProviderException pe) {
							String errMsg = "An error occurred quering Incident in Query-Request. The exception is: "
									+ pe.getMessage();
							OpenEaiException e = new OpenEaiException(errMsg + msgObject);
							return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg,
									"warn");
						}
					}

					Element incidentElement = null;
					List<Element> elementList = new ArrayList<Element>();
					Element responseDataArea = responseXmlObject.getRootElement().getChild(DATA_AREA);
					responseDataArea.removeContent();

					if (incidentObjects != null && incidentObjects.size() > 0) {
						for (XmlEnterpriseObject eObj : incidentObjects) {
							try {
								incidentElement = getElementFromEnterpriseObject(eObj);
							} catch (OpenEaiException oee) {
								String errMsg = "An error occurred converting an OpenEAI object. The exception is: "
										+ oee.getMessage();
								logger.error(LOGTAG + errMsg);
								throw new CommandException(errMsg, oee);
							}
							elementList.add(incidentElement);
						}
						if (!elementList.isEmpty()) {
							logger.info(LOGTAG + "In excute(). Setting data area elements. Elements to set: "
									+ elementList.size());
							for (Element element : elementList) {
								responseDataArea.addContent(element);
							}
						}
					} else {
						logger.info(LOGTAG
								+ "The object returned from Query-Request is empty. The record does not exist on ServiceNow. ");
					}
					String responseMessage = buildReplyDocument(eControlArea, responseXmlObject);
					Message responseToEAI = getMessage(msg, responseMessage);
					return responseToEAI;

				} else if (MessageControl.UPDATE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {

					Element eBaselineData = requestFromEai.getRootElement()
							.getChild(MessageControl.DATA_AREA.toString())
							.getChild(MessageControl.BASELINE_DATA.toString())
							.getChild(MessageControl.INCIDENT_OBJECT.toString());

					Element eNewData = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
							.getChild(MessageControl.NEW_DATA.toString())
							.getChild(MessageControl.INCIDENT_OBJECT.toString());

					if (eNewData == null || eBaselineData == null) {
						String errMsg = "Either the baseline or new data state of the Incident is null. Can't continue.";
						throw new CommandException(errMsg);
					}

					Incident baselineIncident = new Incident();
					Incident servicenowIncident = new Incident();
					try {
						baselineIncident = (Incident) getAppConfig()
								.getObjectByType(baselineIncident.getClass().getName());
						incident = (Incident) getAppConfig().getObjectByType(incident.getClass().getName());
					} catch (EnterpriseConfigurationObjectException ecoe) {
						String errMsg = "An error occurred retrieving an object from " + "AppConfig. The exception is: "
								+ ecoe.getMessage();
						logger.error(LOGTAG + errMsg);
						throw new CommandException(errMsg, ecoe);
					}

					try {
						baselineIncident.buildObjectFromInput(eBaselineData);
						incident.buildObjectFromInput(eNewData);
					} catch (EnterpriseLayoutException ele) {
						String errMsg = "An error occurred building the baseline and newdata states of the Incident object passed in. The exception is: "
								+ ele.getMessage();
						throw new CommandException(errMsg, ele);
					}

					// Perform the baseline check.
					try {
						logger.info(LOGTAG + "incident.getNumber() for update: " + incident.getNumber());
						servicenowIncident = getIncidentProvider().query(incident.getNumber());

						if (servicenowIncident == null) {
							String errDesc = "Baseline is stale and no identity from ServiceNow matches the baseline. No update operation may be performed.";
							logger.warn(LOGTAG + errDesc);
							OpenEaiException e = new OpenEaiException(errDesc + msgObject);
							return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg,
									"warn");
						}

						if (baselineIncident.equals(servicenowIncident)) {
							String errDesc = "Baseline matches the current state of the Incident in the ServiceNow service.";
							logger.info(LOGTAG + errDesc);
						} else {
							String errDesc = "Baseline does not matches the current state of the Incident in the ServiceNow service.";
							logger.info(LOGTAG + errDesc);
						}

						// Verify that the baseline and the new state are not equal.
						if (baselineIncident.equals(incident)) {
							String errDesc = "Baseline state and new state of the objects are equal. No update operation may be performed.";
							logger.warn(LOGTAG + errDesc);
							OpenEaiException e = new OpenEaiException(errDesc + msgObject);
							return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg,
									"error");
						}
					} catch (XmlEnterpriseObjectException xeoe) {
						String errMsg = "An error occurred comparing the baseline and new data. The exception is: "
								+ xeoe.getMessage();
						logger.error(LOGTAG + errMsg);
						throw new CommandException(errMsg, xeoe);
					} catch (ProviderException pe) {
						String errMsg = "An error occurred Querying in Update-Request. The exception is: "
								+ pe.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg, "warn");
					}

					try {
						getIncidentProvider().update(incident);
					} catch (ProviderException pe) {
						String errMsg = "An error occurred Querying in Update-Request. The exception is: "
								+ pe.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg, "warn");
					}

					try {
						MessageProducer producer = getProducerPool().getProducer();
						incident.setBaseline(baselineIncident);
						incident.updateSync((SyncService) producer);
					} catch (EnterpriseObjectSyncException eose) {
						String errMsg = "An error occurred publishing the Incident.Update-Sync message after updating FirewallExceptionRequest. The exception is: "
								+ eose.getMessage();
						logger.error(LOGTAG + errMsg);
						throw new CommandException(errMsg, eose);
					} catch (JMSException jmse) {
						String errMsg = "An error occurred publishing the Incident.Update-Sync message after updating Incident. The exception is: "
								+ jmse.getMessage();
						logger.error(LOGTAG + errMsg);
						throw new CommandException(errMsg, jmse);
					}

				} else if (MessageControl.DELETE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {

					eIncident = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
							.getChild(MessageControl.DELETE_DATA.toString())
							.getChild(MessageControl.INCIDENT_OBJECT.toString());
					if (eIncident == null) {
						String errMsg = "Invalid element found in the Delete-Request message. This command expects a Incident object";
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, errMsg, eControlArea, msgRelease, msg, "error");
					}

					try {
						incident.buildObjectFromInput(eIncident);
					} catch (EnterpriseLayoutException ele) {
						String errMsg = "An error occurred building the delete object from the DataArea element in the Delete-Request message. The exception is: "
								+ ele.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "ServiceNowService-1000", eControlArea, msgRelease, msg,
								"error");
					}

					logger.info(LOGTAG + "delete incident is build: " + incident.toString());

					try {
						getIncidentProvider().delete(incident.getSystemId());
					} catch (ProviderException pe) {
						String errMsg = "An error occurred deleting Incident in Delete-Request message. The exception is: "
								+ pe.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "ServiceNowService-1000", eControlArea, msgRelease, msg,
								"error");
					}

					try {
						MessageProducer producer = getProducerPool().getProducer();
						logger.info(LOGTAG + "Publishing Incident Delete-Sync");
						incident.deleteSync("purge", (SyncService) producer);
						logger.info(LOGTAG + "Published Incident.Delete-Sync" + " message.");
					} catch (EnterpriseObjectSyncException eose) {
						String errMsg = "An error occurred publishing the Incident.Delete-Sync message after deleting Incident.";
						logger.error(LOGTAG + errMsg);
						throw new CommandException(eose);
					} catch (JMSException jmse) {
						String errMsg = "An error occurred publishing the Incident.Delete-Sync message after deleting Incident.";
						logger.error(LOGTAG + errMsg);
						throw new CommandException(jmse);
					}
				} else {
					String errMsg = "The ServiceNow Service doesn't support the input message action. Verify that the sending application is sending appropriate message.";
					OpenEaiException e = new OpenEaiException(errMsg + msgAction);
					return getExceptionResponse(e, errMsg, eControlArea, msgRelease, msg, "error");
				}

				responseXmlObject.getRootElement().getChild(MessageControl.DATA_AREA.getName()).removeContent();
				String replyContents = buildReplyDocument(eControlArea, responseXmlObject);
				return getMessage(msg, replyContents);

			} else {
				String errMsg = "The Servicenow service doesn't support the input message object. Verify that the sending application is sending appropriate message objects. Input message object: ";
				OpenEaiException e = new OpenEaiException(errMsg + msgObject);
				return getExceptionResponse(e, errMsg, eControlArea, msgRelease, msg, "error");
			}
		} catch (Exception e) {
			return getExceptionResponse(e, "an error happened while processing this message", eControlArea, msgRelease, msg, "error");
		}
	}


	private Message processFirewallExceptionAddRequest(TextMessage msg, Document requestFromEai, Document responseXmlObject)
			throws CommandException {
		
		logger.info(LOGTAG + "In processFirewallException Add Request, getting objects: ");
		
		Element eControlArea = getControlArea(requestFromEai.getRootElement());
		String msgAction = this.getMessageAction(requestFromEai);
		String msgObject = this.getMessageObject(requestFromEai);
		String msgRelease = this.getMessageRelease(requestFromEai);
		Element eAuthUserId = eControlArea.getChild("Sender").getChild("Authentication").getChild("AuthUserId");
		String authUserId = eAuthUserId.getValue();
		
		XMLOutputter outputter = new XMLOutputter();
		logger.info("Generate request message: " + outputter.outputString(requestFromEai.getRootElement()));
		
		if (MessageControl.GENERATE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {
			Element eRequisition = null;
			
			logger.info(LOGTAG + "In generate request for FirewallExceptionAddRequest object: ");
			eRequisition = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
					.getChild(MessageControl.FIREWALL_EXCEPTION_ADD_REQUEST_REQUISITION.toString());
	
			if (eRequisition == null) {
				String errMsg = "Invalid element found in the Generate-Request message. This command expects a FirewallExceptionAddRequestRequisition object";
				OpenEaiException e = new OpenEaiException(errMsg + msgObject);
				return getExceptionResponse(e, errMsg, eControlArea, msgRelease, msg, "error");
			}
	
			FirewallExceptionAddRequestRequisition aRequisition = new FirewallExceptionAddRequestRequisition();
			try {
				aRequisition = this.getObject(FirewallExceptionAddRequestRequisition.class);
				aRequisition.buildObjectFromInput(eRequisition);
				logger.info("built Requisition="+aRequisition);
			} catch (EnterpriseLayoutException | EnterpriseConfigurationObjectException ele) {
				String errMsg = "An error occurred building the requisition object from the DataArea element in the Generate-Request message. The exception is: "
						+ ele.getMessage();
				OpenEaiException e = new OpenEaiException(errMsg + msgObject);
				return getExceptionResponse(e, "ServiceNow-1000", eControlArea, msgRelease, msg, "error");
			}
	
			
			FirewallExceptionAddRequest aRequest = new FirewallExceptionAddRequest();
			
			try {
				aRequest = getFirewallExceptionAddProvider().generate(aRequisition);
			} catch (ProviderException pe) {
				String errMsg = "An error occurred generating FirewallExceptionAddRequest in Generate-Request. The exception is: "
						+ pe.getMessage();
				OpenEaiException e = new OpenEaiException(errMsg + msgObject);
				return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg, "error");
			}
	
			// Issue a create sync
//			if (aRequest != null) {
//				try {
//					MessageProducer producer = getProducerPool().getProducer();
//					logger.info(LOGTAG + "Publishing FirewallExceptionAddRequest Create-Sync");
//					Authentication auth = new Authentication();
//					auth.setAuthUserId(authUserId);
//					auth.setAuthUserSignature("none");
//					aRequest.setAuthentication(auth);
//					aRequest.createSync((SyncService) producer);
//					logger.info(LOGTAG + "Published FirewallExceptionAddRequest.Create-Sync" + " message.");
//				} catch (EnterpriseObjectSyncException eose) {
//					String errMsg = "An error occurred publishing the FirewallExceptionAddRequest.Create-Sync message after generating a FirewallExceptionAddRequest.";
//					logger.error(LOGTAG + errMsg);
//					throw new CommandException(eose);
//				} catch (JMSException jmse) {
//					String errMsg = "An error occurred publishing the FirewallExceptionAddRequest.Create-Sync message after generating a FirewallExceptionAddRequest.";
//					logger.error(LOGTAG + errMsg);
//					throw new CommandException(jmse);
//				}
//			}
			
	
			Element elementRequest = null;
			try {
				elementRequest = (Element) aRequest.buildOutputFromObject();
			} catch (EnterpriseLayoutException ele) {
				String errMsg = "An error occurred serializing a AddRequest object to an XML element. The exception is: "
						+ ele.getMessage();
				logger.error(LOGTAG + errMsg, ele);
				throw new CommandException(errMsg, ele);
			}
			
			Element responseELement = responseXmlObject.getRootElement().getChild(MessageControl.DATA_AREA.getName());
			responseELement.removeContent();
			responseELement.addContent(elementRequest);
			
			String replyContents = buildReplyDocument(eControlArea, responseXmlObject);
			
			return getMessage(msg, replyContents);
			
			
		} else if (MessageControl.QUERY_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {
	
			Element eQuerySpec = null;
			eQuerySpec = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
					.getChild(MessageControl.FIREWALL_EXCEPTION_ADD_REQUEST_QUERY_SPECIFICATION.toString());
	
			logger.info("Query messages: " + outputter.outputString(eQuerySpec));
			List<XmlEnterpriseObject> firewallExceptionObjects = null;
			FirewallExceptionAddRequestQuerySpecification queryData = new FirewallExceptionAddRequestQuerySpecification();

			if (eQuerySpec != null) {
				try {
					queryData = this.getObject(FirewallExceptionAddRequestQuerySpecification.class);
					queryData.buildObjectFromInput(eQuerySpec);
				} catch (EnterpriseLayoutException ele) {
					String errMsg = "An error occurred serializing FirewallAddExceptionQuerySpecification object to an XML element. The exception is: "
							+ ele.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new CommandException(errMsg, ele);
				} catch (EnterpriseConfigurationObjectException e) {
					String errMsg = "Error retrieving an object from AppConfig: " + "The exception" + "is: "
							+ e.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new CommandException(errMsg);
				}

				try {
					firewallExceptionObjects = getFirewallExceptionAddProvider().query(queryData);
				} catch (ProviderException pe) {
					String errMsg = "An error occurred quering FirewallExceptionAddRequest in Query-Request. The exception is: "
							+ pe.getMessage();
					OpenEaiException e = new OpenEaiException(errMsg + msgObject);
					return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg, "warn");
				}
			}
	
			Element eAddRequest = null;
			List<Element> elementList = new ArrayList<Element>();
			Element responseDataArea = responseXmlObject.getRootElement().getChild(DATA_AREA);
			responseDataArea.removeContent();
	
			if (firewallExceptionObjects != null && firewallExceptionObjects.size() > 0) {
				for (XmlEnterpriseObject eObj : firewallExceptionObjects) {
					try {
						eAddRequest = getElementFromEnterpriseObject(eObj);
					} catch (OpenEaiException oee) {
						String errMsg = "An error occurred converting an OpenEAI object. The exception is: "
								+ oee.getMessage();
						logger.error(LOGTAG + errMsg);
						throw new CommandException(errMsg, oee);
					}
					elementList.add(eAddRequest);
				}
				if (!elementList.isEmpty()) {
					logger.info(LOGTAG + "In excute(). Setting data area elements. Elements to set: "
							+ elementList.size());
					for (Element element : elementList) {
						responseDataArea.addContent(element);
					}
				}
			} else {
				logger.info(LOGTAG
						+ "The object returned from Query-Request is empty. The record does not exist on ServiceNow. ");
			}
			String responseMessage = buildReplyDocument(eControlArea, responseXmlObject);
			Message responseToEAI = getMessage(msg, responseMessage);
			return responseToEAI;
	
		} else if (MessageControl.UPDATE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {
			
			String errMsg = "Update-request for AddRequest object is not supported";
			logger.warn(errMsg);
			return getExceptionResponse(new OpenEaiException(errMsg), "ServiceNowService-100X", eControlArea, msgRelease, msg, "warn");
			
		} else if (MessageControl.DELETE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {
			
			Element eRequest = null;
			eRequest = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
					.getChild(MessageControl.DELETE_DATA.toString())
					.getChild(MessageControl.FIREWALL_EXCEPTION_ADD_REQUEST.toString());
	
			if (eRequest == null) {
				String errMsg = "Invalid element found in the Delete-Request message. This command expects a FirewallExceptionAddRequest object";
				OpenEaiException e = new OpenEaiException(errMsg + msgObject);
				return getExceptionResponse(e, errMsg, eControlArea, msgRelease, msg, "error");
			}
			
			logger.info("Delete messages: " + outputter.outputString(eRequest));
			
			Document localResponseDoc = (Document) this.getResponseDoc().clone();
			
            logger.info(LOGTAG + "Create response template ... ");
            // Prepare the response.
            if (localResponseDoc.getRootElement().getChild("DataArea") != null) {
                localResponseDoc.getRootElement().getChild("DataArea").removeContent();
            } 
			
			try {
				FirewallExceptionAddRequest aRequest = this.getObject(FirewallExceptionAddRequest.class);
				aRequest.buildObjectFromInput(eRequest);
				this.getFirewallExceptionAddProvider().delete(aRequest);
				
	            String replyContents = buildReplyDocument(eControlArea, localResponseDoc);     
	            // Return the response with status success.
	            return getMessage(msg, replyContents);
	            
			} catch (EnterpriseLayoutException ele) {
	            String errType = "application";
	            String errCode = "OpenEAI-1010";
	            String errDesc = "An error occurred building the delete object from the DataArea element in the Delete-Request message. The exception is: "
						+ ele.getMessage();

	    		List<org.openeai.moa.objects.resources.Error> errors = new ArrayList<org.openeai.moa.objects.resources.Error>();
	            errors.add(buildError(errType, errCode, errDesc));
				String replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
				return getMessage(msg, replyContents);
			} catch (ProviderException e) {
	            String errType = "application";
	            String errCode = "ServiceNowService-1001";
	            String errDesc = "An error occurred in ServiceNow for the Delete-Request message. The error is: "
						+ e.getMessage();

	    		List<org.openeai.moa.objects.resources.Error> errors = new ArrayList<org.openeai.moa.objects.resources.Error>();
	            errors.add(buildError(errType, errCode, errDesc));
				String replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
				return getMessage(msg, replyContents);
			} catch (EnterpriseConfigurationObjectException e) {
	            String errType = "application";
	            String errCode = "ServiceNowService-2001";
	            String errDesc = "ServiceNowService AppConfig does not configuired for FirewallExceptionAddRequest. The error is: "
						+ e.getMessage();

	    		List<org.openeai.moa.objects.resources.Error> errors = new ArrayList<org.openeai.moa.objects.resources.Error>();
	            errors.add(buildError(errType, errCode, errDesc));
				String replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
				return getMessage(msg, replyContents);
			}
	
			//upon successful delete, publish delete sync
//			try {
//				MessageProducer producer = getProducerPool().getProducer();
//				logger.info(LOGTAG + "Publishing FirewallException Delete-Sync");
//				aRequest.deleteSync("delete", (SyncService) producer);
//				logger.info(LOGTAG + "Published FirewallExceptionRemoveRequest.Delete-Sync" + " message.");
//			} catch (EnterpriseObjectSyncException eose) {
//				String errMsg = "An error occurred publishing the FirewallExceptionRemoveRequest.Delete-Sync message after deleting FirewallException.";
//				logger.error(LOGTAG + errMsg);
//				throw new CommandException(eose);
//			} catch (JMSException jmse) {
//				String errMsg = "An error occurred publishing the FirewallExceptionRemoveRequest.Delete-Sync message after deleting FirewallException.";
//				logger.error(LOGTAG + errMsg);
//				throw new CommandException(jmse);
//			}
		
            
		} else {
			String errMsg = "The ServiceNow Service doesn't support the input message action. Verify that the sending application is sending appropriate message.";
			OpenEaiException e = new OpenEaiException(errMsg + msgAction);
			return getExceptionResponse(e, errMsg, eControlArea, msgRelease, msg, "error");
		}
	
//		responseXmlObject.getRootElement().getChild(MessageControl.DATA_AREA.getName()).removeContent();
//		String replyContents = buildReplyDocument(eControlArea, responseXmlObject);
//		return getMessage(msg, replyContents);
	}


	private Message processFirewallExceptionRemoveRequest(TextMessage msg, Document requestFromEai, Document responseXmlObject)
			throws CommandException {
		
		Element eControlArea = getControlArea(requestFromEai.getRootElement());
		String msgAction = this.getMessageAction(requestFromEai);
		String msgObject = this.getMessageObject(requestFromEai);
		String msgRelease = this.getMessageRelease(requestFromEai);
		Element eAuthUserId = eControlArea.getChild("Sender").getChild("Authentication").getChild("AuthUserId");
		String authUserId = eAuthUserId.getValue();
		
		logger.info(LOGTAG + "In processFirewallException Remove Request, getting objects: ");
		
		if (MessageControl.GENERATE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {
			Element eRequisition = null;
			
			logger.info(LOGTAG + "In generate request for FirewallExceptionRemoveRequest object: ");
			eRequisition = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
					.getChild(MessageControl.FIREWALL_EXCEPTION_REMOVE_REQUEST_REQUISITION.toString());
	
			if (eRequisition == null) {
				String errMsg = "Invalid element found in the Generate-Request message. This command expects a FirewallExceptionRemoveRequestRequisition object";
				OpenEaiException e = new OpenEaiException(errMsg + msgObject);
				return getExceptionResponse(e, errMsg, eControlArea, msgRelease, msg, "error");
			}
			
			XMLOutputter outputter = new XMLOutputter();
			logger.info("Generate message: " + outputter.outputString(eRequisition));
	
			FirewallExceptionRemoveRequestRequisition aRequisition = new FirewallExceptionRemoveRequestRequisition();
			try {
				aRequisition = this.getObject(FirewallExceptionRemoveRequestRequisition.class);
				aRequisition.buildObjectFromInput(eRequisition);
			} catch (EnterpriseLayoutException | EnterpriseConfigurationObjectException ele) {
				String errMsg = "An error occurred building the requisition object from the DataArea element in the Generate-Request message. The exception is: "
						+ ele.getMessage();
				OpenEaiException e = new OpenEaiException(errMsg + msgObject);
				return getExceptionResponse(e, "ServiceNow-1000", eControlArea, msgRelease, msg, "error");
			}
	
			
			FirewallExceptionRemoveRequest aRequest = new FirewallExceptionRemoveRequest();
			
			try {
				aRequest = getFirewallExceptionRemoveProvider().generate(aRequisition);
			} catch (ProviderException pe) {
				String errMsg = "An error occurred generating FirewallException in Generate-Request. The exception is: "
						+ pe.getMessage();
				OpenEaiException e = new OpenEaiException(errMsg + msgObject);
				return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg, "error");
			}
	
			// Issue a create sync
			if (aRequest != null) {
				try {
					MessageProducer producer = getProducerPool().getProducer();
					logger.info(LOGTAG + "Publishing FirewallExceptionRequest Create-Sync");
					Authentication auth = new Authentication();
					auth.setAuthUserId(authUserId);
					auth.setAuthUserSignature("none");
					aRequest.setAuthentication(auth);
					aRequest.createSync((SyncService) producer);
					logger.info(LOGTAG + "Published FirewallExceptionRemoveRequest.Create-Sync" + " message.");
				} catch (EnterpriseObjectSyncException eose) {
					String errMsg = "An error occurred publishing the FirewallExceptionRemoveRequest.Create-Sync message after generating an identity.";
					logger.error(LOGTAG + errMsg);
					throw new CommandException(eose);
				} catch (JMSException jmse) {
					String errMsg = "An error occurred publishing the FirewallExceptionRemoveRequest.Create-Sync message after generating an identity.";
					logger.error(LOGTAG + errMsg);
					throw new CommandException(jmse);
				}
			}
			logger.info(LOGTAG + "In execute(). Completed returning oEAI object and publishing Create-Sync for RemoveRequest.");
	
			Element elementRequest = null;
			try {
				elementRequest = (Element) aRequest.buildOutputFromObject();
			} catch (EnterpriseLayoutException ele) {
				String errMsg = "An error occurred serializing a RemoveRequest object to an XML element. The exception is: "
						+ ele.getMessage();
				logger.error(LOGTAG + errMsg, ele);
				throw new CommandException(errMsg, ele);
			}
			
			Element responseELement = responseXmlObject.getRootElement().getChild(MessageControl.DATA_AREA.getName());
			responseELement.removeContent();
			responseELement.addContent(elementRequest);
			
			String replyContents = buildReplyDocument(eControlArea, responseXmlObject);
			return getMessage(msg, replyContents);
			
			
		} else if (MessageControl.QUERY_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {
	
			Element eQuerySpec = null;
			eQuerySpec = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
					.getChild(MessageControl.FIREWALL_EXCEPTION_REMOVE_REQUEST_QUERY_SPECIFICATION.toString());
	
			XMLOutputter outputter = new XMLOutputter();
			logger.info("Query messages: " + outputter.outputString(eQuerySpec));
			List<XmlEnterpriseObject> firewallExceptionObjects = null;
			FirewallExceptionRemoveRequestQuerySpecification queryData = new FirewallExceptionRemoveRequestQuerySpecification();

			if (eQuerySpec != null) {
				try {
					queryData = this.getObject(FirewallExceptionRemoveRequestQuerySpecification.class);
					queryData.buildObjectFromInput(eQuerySpec);
				} catch (EnterpriseLayoutException ele) {
					String errMsg = "An error occurred serializing FirewallRemoveException object to an XML element. The exception is: "
							+ ele.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new CommandException(errMsg, ele);
				} catch (EnterpriseConfigurationObjectException e) {
					String errMsg = "Error retrieving an object from AppConfig: " + "The exception" + "is: "
							+ e.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new CommandException(errMsg);
				}

				try {
					firewallExceptionObjects = getFirewallExceptionRemoveProvider().query(queryData);
				} catch (ProviderException pe) {
					String errMsg = "An error occurred quering FirewallException in Query-Request. The exception is: "
							+ pe.getMessage();
					OpenEaiException e = new OpenEaiException(errMsg + msgObject);
					return getExceptionResponse(e, "ServiceNowService-100X", eControlArea, msgRelease, msg, "warn");
				}
			}
	
			Element eRemoveRequest = null;
			List<Element> elementList = new ArrayList<Element>();
			Element responseDataArea = responseXmlObject.getRootElement().getChild(DATA_AREA);
			responseDataArea.removeContent();
	
			if (firewallExceptionObjects != null && firewallExceptionObjects.size() > 0) {
				for (XmlEnterpriseObject eObj : firewallExceptionObjects) {
					try {
						eRemoveRequest = getElementFromEnterpriseObject(eObj);
					} catch (OpenEaiException oee) {
						String errMsg = "An error occurred converting an OpenEAI object. The exception is: "
								+ oee.getMessage();
						logger.error(LOGTAG + errMsg);
						throw new CommandException(errMsg, oee);
					}
					elementList.add(eRemoveRequest);
				}
				if (!elementList.isEmpty()) {
					logger.info(LOGTAG + "In excute(). Setting data area elements. Elements to set: "
							+ elementList.size());
					for (Element element : elementList) {
						responseDataArea.addContent(element);
					}
				}
			} else {
				logger.info(LOGTAG
						+ "The object returned from Query-Request is empty. The record does not exist on ServiceNow. ");
			}
			String responseMessage = buildReplyDocument(eControlArea, responseXmlObject);
			Message responseToEAI = getMessage(msg, responseMessage);
			return responseToEAI;
	
		} else if (MessageControl.UPDATE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {
			
			String errMsg = "Update-request for RemoveRequest object is not supported";
			logger.warn(errMsg);
			return getExceptionResponse(new OpenEaiException(errMsg), "ServiceNowService-100X", eControlArea, msgRelease, msg, "warn");
			
		} else if (MessageControl.DELETE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {
			
			Element eRequest = null;
			eRequest = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
					.getChild(MessageControl.DELETE_DATA.toString())
					.getChild(MessageControl.FIREWALL_EXCEPTION_REMOVE_REQUEST.toString());
	
			if (eRequest == null) {
				String errMsg = "Invalid element found in the Delete-Request message. This command expects a FirewallExceptionRemoveRequest object";
				OpenEaiException e = new OpenEaiException(errMsg + msgObject);
				return getExceptionResponse(e, errMsg, eControlArea, msgRelease, msg, "error");
			}
			
			XMLOutputter outputter = new XMLOutputter();
			logger.info("Delete messages: " + outputter.outputString(eRequest));
			
			Document localResponseDoc = (Document) this.getResponseDoc().clone();
			
            logger.info(LOGTAG + "Create response template ... ");
            // Prepare the response.
            if (localResponseDoc.getRootElement().getChild("DataArea") != null) {
                localResponseDoc.getRootElement().getChild("DataArea").removeContent();
            } 
			
			
			try {
				FirewallExceptionRemoveRequest aRequest = this.getObject(FirewallExceptionRemoveRequest.class);
				
				aRequest.buildObjectFromInput(eRequest);
				this.getFirewallExceptionRemoveProvider().delete(aRequest);
				
	            String replyContents = buildReplyDocument(eControlArea, localResponseDoc);     
	            // Return the response with status success.
	            return getMessage(msg, replyContents);
	            
			} catch (EnterpriseLayoutException ele) {
	            String errType = "application";
	            String errCode = "OpenEAI-1010";
	            String errDesc = "An error occurred building the delete object from the DataArea element in the Delete-Request message. The exception is: "
						+ ele.getMessage();
	            logger.error(errDesc, ele);

	    		List<org.openeai.moa.objects.resources.Error> errors = new ArrayList<org.openeai.moa.objects.resources.Error>();
	            errors.add(buildError(errType, errCode, errDesc));
				String replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
				return getMessage(msg, replyContents);
			} catch (ProviderException e) {
	            String errType = "application";
	            String errCode = "ServiceNowService-1001";
	            String errDesc = "An error occurred in ServiceNow for the Delete-Request message. The error is: "
						+ e.getMessage();

	    		List<org.openeai.moa.objects.resources.Error> errors = new ArrayList<org.openeai.moa.objects.resources.Error>();
	            errors.add(buildError(errType, errCode, errDesc));
				String replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
				return getMessage(msg, replyContents);
			} catch (EnterpriseConfigurationObjectException e) {
				String errType = "application";
	            String errCode = "ServiceNowService-200x";
	            String errDesc = "ServiceNowService AppConfig error.  Cannot locate configuration for FirewallExceptionRemoveRequest. The exception is: "
						+ e.getMessage();
	            logger.error(errDesc, e);

	    		List<org.openeai.moa.objects.resources.Error> errors = new ArrayList<org.openeai.moa.objects.resources.Error>();
	            errors.add(buildError(errType, errCode, errDesc));
				String replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
				return getMessage(msg, replyContents);
			}
	
			//upon successful delete, publish delete sync
//			try {
//				MessageProducer producer = getProducerPool().getProducer();
//				logger.info(LOGTAG + "Publishing FirewallException Delete-Sync");
//				aRequest.deleteSync("delete", (SyncService) producer);
//				logger.info(LOGTAG + "Published FirewallExceptionRemoveRequest.Delete-Sync" + " message.");
//			} catch (EnterpriseObjectSyncException eose) {
//				String errMsg = "An error occurred publishing the FirewallExceptionRemoveRequest.Delete-Sync message after deleting FirewallException.";
//				logger.error(LOGTAG + errMsg);
//				throw new CommandException(eose);
//			} catch (JMSException jmse) {
//				String errMsg = "An error occurred publishing the FirewallExceptionRemoveRequest.Delete-Sync message after deleting FirewallException.";
//				logger.error(LOGTAG + errMsg);
//				throw new CommandException(jmse);
//			}
		
            
		} else {
			String errMsg = "The ServiceNow Service doesn't support the input message action. Verify that the sending application is sending appropriate message.";
			OpenEaiException e = new OpenEaiException(errMsg + msgAction);
			return getExceptionResponse(e, errMsg, eControlArea, msgRelease, msg, "error");
		}
	
//		responseXmlObject.getRootElement().getChild(MessageControl.DATA_AREA.getName()).removeContent();
//		String replyContents = buildReplyDocument(eControlArea, responseXmlObject);
//		return getMessage(msg, replyContents);
	}

	/**
	 * 
	 * @param msgAction
	 * @param msgObject
	 * @param msgRelease
	 * @return
	 * @throws CommandException
	 */
	protected Document getResponseXml(final String msgAction, final String msgObject, final String msgRelease)
			throws CommandException {
		try {
			XmlEnterpriseObject xmlObject = (XmlEnterpriseObject) getAppConfig()
					.getObject(msgObject + "." + generateRelease(msgRelease));
			Document responseObject = null;
			
			responseObject = xmlObject.getProvideDoc();
			
			if (responseObject == null) {
				String errMsg = "Could not find an appropriate reply document for the following request: "+msgAction;
				throw new CommandException(errMsg + msgObject + "-" + msgAction);
			}
			return responseObject;
		} catch (Exception e) {
			throw new CommandException(e.getMessage(), e);
		}
	}

	/**
	 * This method wraps the response exception passed in to the Error elements
	 * as specified in the OpenEAI protocol.
	 * 
	 * @param e
	 * @param errorId
	 * @param eControlArea
	 * @param msgRelease
	 * @param msg
	 * @return
	 * @throws CommandException
	 */
	protected Message getExceptionResponse(Exception e, String errorId, Element eControlArea, String msgRelease,
			TextMessage msg, String logLevel) throws CommandException {
		String msgaction = this.getMessageAction(eControlArea.getDocument());
		Document xmlObject = getResponseXml(msgaction,
				eControlArea.getAttributeValue(MESSAGE_OBJECT), msgRelease);

		List<org.openeai.moa.objects.resources.Error> errors = new ArrayList<org.openeai.moa.objects.resources.Error>();
		errors.add(buildError(e.getClass().getName(), "EaiException" + errorId, e.getMessage()));

		String responseContents = buildReplyDocumentWithErrors(eControlArea, xmlObject, errors);
		if (logLevel.equals("error")) {
			logger.error(LOGTAG + "Exception Thrown: \nException Message: " + e.getMessage() + "\nStack Trace: "
					+ this.getExceptionStackTrace(e));
		} else {
			logger.warn(LOGTAG + "Exception Thrown: \nException Message: " + e.getMessage());
		}
		return getMessage(msg, responseContents);
	}

	private String getExceptionStackTrace(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString();
	}

	public static int getConnectionTimeout() {
		return connectionTimeout;
	}

	public static void setConnectionTimeout(int connectionTimeout) {
		ServicenowServiceRequestCommand.connectionTimeout = connectionTimeout;
	}

	public static int getSocketTimeout() {
		return socketTimeout;
	}

	public static void setSocketTimeout(int socketTimeout) {
		ServicenowServiceRequestCommand.socketTimeout = socketTimeout;
	}

	protected static void setServicePassword(String password) {
		servicePassword = password;
	}

	public static String getServicePassword() {
		return servicePassword;
	}

	protected static void setServiceUserId(String id) {
		serviceUserId = id;
	}

	public static String getServiceUserId() {
		return serviceUserId;
	}

	protected static void setWebServiceEndpoint(String endpoint) {
		searchServiceEndpoint = endpoint;
	}

	protected static String getWebServiceEndpoint() {
		return searchServiceEndpoint;
	}

	public ProducerPool getProducerPool() {
		return this.producerPool;
	}

	public void setProducerPool(ProducerPool producerPool) {
		this.producerPool = producerPool;
	}

	public Document getResponseDoc() {
		return responseDoc;
	}

	public void setResponseDoc(Document responseDoc) {
		this.responseDoc = responseDoc;
	}

	public IncidentProvider getIncidentProvider() {
		return incidentProvider;
	}

	public void setIncidentProvider(IncidentProvider incidentProvider) {
		this.incidentProvider = incidentProvider;
	}
//
//	public FirewallExceptionRequestProvider getFirewallExceptionProvider() {
//		return firewallExceptionProvider;
//	}
//
//	public void setFirewallExceptionProvider(FirewallExceptionRequestProvider firewallExceptionProvider) {
//		this.firewallExceptionProvider = firewallExceptionProvider;
//	}

	public ElasticIpRequestProvider getElasticIPProvider() {
		return elasticIPProvider;
	}

	public void setElasticIPProvider(ElasticIpRequestProvider elasticIPProvider) {
		this.elasticIPProvider = elasticIPProvider;
	}

	@SuppressWarnings("unchecked")
	private <T extends XmlEnterpriseObjectImpl> T getObject(Class<T> aClass)
			throws EnterpriseConfigurationObjectException {
		return (T) getAppConfig().getObjectByType(aClass.getName());
	}

	private <T> Element getElementFromEnterpriseObject(T eObject) throws OpenEaiException {

		if (!(eObject instanceof org.openeai.moa.XmlEnterpriseObject)) {
			String errMsg = "Error while attempting to convert a MOA Enterprise Object to JDOM Element. Input object not of type 'org.openeai.moa.XmlEnterpriseObject'.";
			throw new OpenEaiException(errMsg);
		}

		XmlEnterpriseObject enterpriseObj = (XmlEnterpriseObject) eObject;
		Element eOutput = null;
		if (enterpriseObj != null) {
			org.openeai.layouts.EnterpriseLayoutManager outElm = enterpriseObj
					.getOutputLayoutManager(MessageControl.XML_OUTPUT.toString());
			enterpriseObj.setOutputLayoutManager(outElm);
			eOutput = (Element) enterpriseObj.buildOutputFromObject();
		}
		return eOutput;
	}
}
