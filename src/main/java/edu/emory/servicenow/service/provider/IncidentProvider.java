package edu.emory.servicenow.service.provider;

import java.util.List;

import org.openeai.config.AppConfig;
import org.openeai.moa.XmlEnterpriseObject;

import com.service_now.moa.jmsobjects.servicedesk.v2_0.Incident;
import com.service_now.moa.objects.resources.v2_0.IncidentQuerySpecification;
import com.service_now.moa.objects.resources.v2_0.IncidentRequisition;

public abstract interface IncidentProvider {
	
	void init(AppConfig appConfig) throws ProviderException;
	List<XmlEnterpriseObject> query(IncidentQuerySpecification queryData) throws ProviderException;
	Incident generate(IncidentRequisition incidentRequisition) throws ProviderException;
	void delete(String id) throws ProviderException;
	void update(Incident incident) throws ProviderException;
	Incident query(String number) throws ProviderException;
	
}
