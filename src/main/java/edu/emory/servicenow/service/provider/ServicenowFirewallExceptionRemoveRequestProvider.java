package edu.emory.servicenow.service.provider;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.moa.XmlEnterpriseObject;

import com.service_now.moa.jmsobjects.customrequests.v1_0.FirewallExceptionRemoveRequest;
import com.service_now.moa.objects.resources.v1_0.FirewallExceptionRemoveRequestQuerySpecification;
import com.service_now.moa.objects.resources.v1_0.FirewallExceptionRemoveRequestRequisition;
import com.service_now.www.u_esb_firewall_request_crud.GetRecordsResponseDocument;
import com.service_now.www.u_esb_firewall_request_crud.GetRecordsResponseDocument.GetRecordsResponse;
import com.service_now.www.u_esb_firewall_request_crud.GetRecordsResponseDocument.GetRecordsResponse.GetRecordsResult;
import com.service_now.www.u_esb_firewall_request_crud.InsertResponseDocument;

import edu.emory.servicenow.service.client.FirewallExceptionRequestClient;
import edu.emory.servicenow.service.util.ServiceNowValues;
import edu.emory.servicenow.service.util.StringUtil;

/**
 * Provider implementation for ServicenowFirewallRemoveExceptionProvider
 * 
 * @author RXING2
 * @author nchen3
 */
public class ServicenowFirewallExceptionRemoveRequestProvider extends ServicenowFirewallExceptionRequestBaseProvider
		implements FirewallExceptionRemoveRequestProvider {
	private static Logger logger = Logger.getLogger(ServicenowFirewallExceptionRemoveRequestProvider.class);
	private String LOGTAG = "[ServicenowFirewallExceptionRemoveRequestProvider] ";

	private List<XmlEnterpriseObject> getFirewallExceptionRemoveRequestResponse(GetRecordsResponse object)
			throws EnterpriseConfigurationObjectException, EnterpriseFieldException, ProviderException {

		List<XmlEnterpriseObject> firewallExceptionObjects = new ArrayList<XmlEnterpriseObject>();
		
		for (GetRecordsResult recordsResult : object.getGetRecordsResultArray()) {
			
			String requestType = recordsResult.getUNeedFirewall();
			logger.info(LOGTAG + "result record needFirewall: "+ requestType);
					
			if (ServiceNowValues.REMOVE_EXCEPTION.getName().equals(requestType)) {
				
				FirewallExceptionRemoveRequest removeRequest = getObject(FirewallExceptionRemoveRequest.class);

				if (recordsResult.getSysId() != null && !recordsResult.getSysId().isEmpty()) {
					removeRequest.setSystemId(recordsResult.getSysId());
					logger.info("recordsResult.getSysId(): " + recordsResult.getSysId());
				}
				
				if (recordsResult.getURitmNumber() != null && !recordsResult.getURitmNumber().isEmpty()) {
					removeRequest.setRequestItemNumber(recordsResult.getURitmNumber());
					logger.info("recordsResult.getURitmNumber(): " + recordsResult.getURitmNumber());
				}

				if (recordsResult.getURitmState() != null && !recordsResult.getURitmState().isEmpty()) {
					removeRequest.setRequestItemState(recordsResult.getURitmState());
					logger.info("recordsResult.getURitmState(): " + recordsResult.getURitmState());
				}
				
				if (recordsResult.getUReqState() != null && !recordsResult.getUReqState().isEmpty()) {
					removeRequest.setRequestState(recordsResult.getUReqState());
					logger.info("recordsResult.getUReqState(): " + recordsResult.getUReqState());
				}
				
				if (recordsResult.getURemoveWhat() != null && !recordsResult.getURemoveWhat().isEmpty()) {
					removeRequest.setRequestDetails(recordsResult.getURemoveWhat());
					logger.info("recordsResult.getURemoveWhat(): " + recordsResult.getURemoveWhat());
				}
				
				if (recordsResult.getUTag() != null && !recordsResult.getUTag().isEmpty()) {
					StringTokenizer tokenizer = new StringTokenizer(recordsResult.getUTag(), ",");
					while (tokenizer.hasMoreTokens()) {
						removeRequest.addTag(tokenizer.nextToken());
					}
					logger.info("recordsResult.getUTag(): " + recordsResult.getUTag());
				}
				
				if(StringUtils.isNotBlank(recordsResult.getUTechContact())) {
					removeRequest.setUserNetID(recordsResult.getUTechContact());
				} else if (StringUtils.isNotBlank(recordsResult.getURequestedFor())) {
					removeRequest.setUserNetID(recordsResult.getURequestedFor());
				} else {
					removeRequest.setUserNetID("UNKNOWN");
				}
				
				firewallExceptionObjects.add(removeRequest);
			}

		}
		
		return firewallExceptionObjects;
	}

	@Override
	public FirewallExceptionRemoveRequest query(String itemNumber) throws ProviderException {
		FirewallExceptionRemoveRequest removeRequest = null;
		
		logger.info(LOGTAG + "Ready to execute FirewallExceptionRemoveRequest.Query-Request. RITM: "+itemNumber);
		
		if (itemNumber != null && itemNumber.length() > 0) {
			
			FirewallExceptionRemoveRequestQuerySpecification querySpec = new FirewallExceptionRemoveRequestQuerySpecification();
			try {
				querySpec.setRequestItemNumber(itemNumber);
			} catch (EnterpriseFieldException e) {
				String errMsg = "An error occurred when setRequestItemNumber(): "+itemNumber;
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, e);
			}
			
			List<XmlEnterpriseObject> results = this.query(querySpec);
			
			if (results != null && results.size()>0) {
				removeRequest = (FirewallExceptionRemoveRequest) results.get(0);
			}
			
		} 
		
		return removeRequest;
	}
	
	
	@Override
	public List<XmlEnterpriseObject> query(FirewallExceptionRemoveRequestQuerySpecification querySpec)
			throws ProviderException {

		logger.info(LOGTAG + "Ready to execute FirewallExceptionRemoveRequest.Query-Request."+querySpec);
		GetRecordsResponseDocument getRecordsResponseDocument = GetRecordsResponseDocument.Factory.newInstance();
		try {
			getRecordsResponseDocument = FirewallExceptionRequestClient.doGetRecordsSearch(this.getFirewallExceptionService(),
					querySpec);
		} catch (RemoteException re) {
			String errMsg = "An error occurred calling ServiceNow from query(), please check the log for more detail";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}

		List<XmlEnterpriseObject> responseMoas = null;
		if (getRecordsResponseDocument != null && getRecordsResponseDocument.getGetRecordsResponse() != null
				&& getRecordsResponseDocument.getGetRecordsResponse().getGetRecordsResultArray().length > 0) {

			GetRecordsResponse resultData = getRecordsResponseDocument.getGetRecordsResponse();
			try {
				responseMoas = getFirewallExceptionRemoveRequestResponse(resultData);
			} catch (EnterpriseConfigurationObjectException ecoe) {
				String errMsg = "An error occurred retrieving an object from ServiceNow client during doFirewallExceptionGrant()";
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, ecoe);
			} catch (EnterpriseFieldException efe) {
				String errMsg = "An error occurred retrieving an object from ServiceNow client during doFirewallExceptionGrant()";
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, efe);
			}

		}
		return responseMoas;
	}

	@Override
	public void delete(FirewallExceptionRemoveRequest aRequest) throws ProviderException {

		logger.info(LOGTAG + "Ready to execute FirewallExceptionRemoveRequest.Delete-Request.");
		logger.info(LOGTAG + "Check to see whether the request exists in ServiceNow...");
		FirewallExceptionRemoveRequest current = this.query(aRequest.getRequestItemNumber());	
		logger.info("this corresponding request in ServiceNow is: "+ current);
		
		if (current != null) {
			if (ServiceNowValues.OPEN_STATE.getName().equals(current.getRequestItemState())) {

				try {
					// the delete web service call will do cancel instead
					FirewallExceptionRequestClient.doFirewallExceptionDelete(this.getFirewallExceptionService(), current.getSystemId());

				} catch (RemoteException re) {
					String errMsg = "An error occurred calling ServiceNow to delete a FirewallExceptionRemoveRequest, please check the log for more details";
					logger.error((LOGTAG + errMsg));
					throw new ProviderException(errMsg, re);
				}

			} else if (ServiceNowValues.CLOSED_CANCEL_STATE.getName().equals(current.getRequestState())) {
				logger.info("The state of this request is "+current.getRequestState());
				throw new ProviderException("The request has been cancelled and cannot be deleted.");
			} else {
				logger.info("The state of this request is "+current.getRequestState());
				throw new ProviderException("The request has been worked on and cannot be deleted.");
			}
		
		} else {
			logger.info("The request does not exist in ServiceNow");
			throw new ProviderException("The request does not exist in ServiceNow.");
		}

		return;
	}

	@Override
	public FirewallExceptionRemoveRequest generate(FirewallExceptionRemoveRequestRequisition requestRequisition) throws ProviderException {
		logger.info(LOGTAG + "Ready to execute FirewallExceptionRemoveRequest.Generate-Request.");

		String itemNumber = null;

		try {
			InsertResponseDocument insertResponseDocument = null;
			insertResponseDocument = FirewallExceptionRequestClient.doFirewallExceptionCreate(this.getFirewallExceptionService(),
					requestRequisition);
			if ((insertResponseDocument != null && insertResponseDocument.getInsertResponse() != null
					&& insertResponseDocument.getInsertResponse().getStatusMessage() != null
					&& !insertResponseDocument.getInsertResponse().getStatusMessage().isEmpty())) {

				String[] statusMessage = StringUtil
						.splitString(insertResponseDocument.getInsertResponse().getStatusMessage());
				itemNumber = statusMessage[0];

			}
		} catch (RemoteException re) {
			String errMsg = "An error occurred calling ServiceNow from insert(), please check the log for more detail";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}
		
		FirewallExceptionRemoveRequest removeRequest = null;
		if (itemNumber != null && itemNumber.length() > 0) {
			logger.info("Item number from FirewallExceptionRemoveRequest.Generate-Request " + itemNumber);
			removeRequest = this.query(itemNumber);
		} 
		
		return removeRequest;
	}
}