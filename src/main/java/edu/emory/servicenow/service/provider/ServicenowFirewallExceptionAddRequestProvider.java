package edu.emory.servicenow.service.provider;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.moa.XmlEnterpriseObject;

import com.service_now.moa.jmsobjects.customrequests.v1_0.FirewallExceptionAddRequest;
import com.service_now.moa.objects.resources.v1_0.Date;
import com.service_now.moa.objects.resources.v1_0.FirewallExceptionAddRequestQuerySpecification;
import com.service_now.moa.objects.resources.v1_0.FirewallExceptionAddRequestRequisition;
import com.service_now.www.u_esb_firewall_request_crud.GetRecordsResponseDocument;
import com.service_now.www.u_esb_firewall_request_crud.GetRecordsResponseDocument.GetRecordsResponse;
import com.service_now.www.u_esb_firewall_request_crud.GetRecordsResponseDocument.GetRecordsResponse.GetRecordsResult;
import com.service_now.www.u_esb_firewall_request_crud.InsertResponseDocument;

import edu.emory.servicenow.service.client.FirewallExceptionRequestClient;
import edu.emory.servicenow.service.util.CalendarUtil;
import edu.emory.servicenow.service.util.ServiceNowValues;
import edu.emory.servicenow.service.util.StringUtil;

/**
 * Provider implementation for ServicenowFirewallExceptionAddRequestProvider
 * 
 * @author RXING2
 * @author nchen3
 * 
 */
public class ServicenowFirewallExceptionAddRequestProvider extends ServicenowFirewallExceptionRequestBaseProvider
		implements FirewallExceptionAddRequestProvider {
	private static Logger logger = Logger.getLogger(ServicenowFirewallExceptionAddRequestProvider.class);
	private String LOGTAG = "[ServicenowFirewallExceptionAddRequestProvider] ";

	private List<XmlEnterpriseObject> getFirewallExceptionResponse(GetRecordsResponse object)
			throws EnterpriseConfigurationObjectException, EnterpriseFieldException, ProviderException {

		List<XmlEnterpriseObject> addRequests = new ArrayList<XmlEnterpriseObject>();
		for (GetRecordsResult recordsResult : object.getGetRecordsResultArray()) {
			
			String requestType = recordsResult.getUNeedFirewall();
			logger.info(LOGTAG + "result record needFirewall: "+ requestType);
					
			if (ServiceNowValues.ADD_EXCEPTION.getName().equals(requestType)) {		
				
				FirewallExceptionAddRequest firewallExceptionObject = this.getObject(FirewallExceptionAddRequest.class);
				
				if (recordsResult.getSysId() != null && !recordsResult.getSysId().isEmpty()) {
					firewallExceptionObject.setSystemId(recordsResult.getSysId());
					logger.info("recordsResult..getSysId(): " + recordsResult.getSysId());
				}
				if (recordsResult.getURitmNumber() != null && !recordsResult.getURitmNumber().isEmpty()) {
					firewallExceptionObject.setRequestItemNumber(recordsResult.getURitmNumber());
					logger.info("recordsResult.getURitmNumber(): " + recordsResult.getURitmNumber());
				}
				if (recordsResult.getURitmState() != null && !recordsResult.getURitmState().isEmpty()) {
					firewallExceptionObject.setRequestItemState(recordsResult.getURitmState());
					logger.info("recordsResult.getURitmState(): " + recordsResult.getURitmState());
				}
				if (recordsResult.getUReqState() != null && !recordsResult.getUReqState().isEmpty()) {
					firewallExceptionObject.setRequestState(recordsResult.getUReqState());
					logger.info("recordsResult.getUReqState(): " + recordsResult.getUReqState());
				}
				
				if (recordsResult.getUTechContact() != null && !recordsResult.getUTechContact().isEmpty()) {
					firewallExceptionObject.setUserNetID(recordsResult.getUTechContact());
					logger.info("recordsResult.getUTechContact(): " + recordsResult.getUTechContact());
				}
				if (recordsResult.getUCommonName() != null && !recordsResult.getUCommonName().isEmpty()) {
					firewallExceptionObject.setApplicationName(recordsResult.getUCommonName());
					logger.info("recordsResult.getUCommonName(): " + recordsResult.getUCommonName());
				}
				if (recordsResult.getUInternetAccess() != null && !recordsResult.getUInternetAccess().isEmpty()) {
					firewallExceptionObject.setSourceOutsideEmory(recordsResult.getUInternetAccess());
					logger.info("recordsResult.getUInternetAccess(): " + recordsResult.getUInternetAccess());
				}
				if (recordsResult.getUHowLong() != null && !recordsResult.getUHowLong().isEmpty()) {
					firewallExceptionObject.setTimeRule(recordsResult.getUHowLong());
					logger.info("recordsResult.getUHowLong(): " + recordsResult.getUHowLong());
				}
	
				if (recordsResult.getUIpAddress() != null && !recordsResult.getUIpAddress().isEmpty()) {
					firewallExceptionObject.setSourceIpAddresses(recordsResult.getUIpAddress());
					logger.info("recordsResult.getUIpAddress(): " + recordsResult.getUIpAddress());
				}
	
				if (recordsResult.getUIpAddress2() != null && !recordsResult.getUIpAddress2().isEmpty()) {
					firewallExceptionObject.setDestinationIpAddresses(recordsResult.getUIpAddress2());
					logger.info("recordsResult.getUIpAddress2(): " + recordsResult.getUIpAddress2());
				}
				if (recordsResult.getUPorts() != null && !recordsResult.getUPorts().isEmpty()) {
					firewallExceptionObject.setPorts(recordsResult.getUPorts());
					logger.info("recordsResult.getUPorts(): " + recordsResult.getUPorts());
				}
				if (recordsResult.getUBusinessReason() != null && !recordsResult.getUBusinessReason().isEmpty()) {
					firewallExceptionObject.setBusinessReason(recordsResult.getUBusinessReason());
					logger.info("recordsResult.getUBusinessReason(): " + recordsResult.getUBusinessReason());
				}
				
				
				if (recordsResult.getUPatchServer() != null && !recordsResult.getUPatchServer().isEmpty()) {
					firewallExceptionObject.setPatched(recordsResult.getUPatchServer());
					logger.info("recordsResult.getUPatchServer(): " + recordsResult.getUPatchServer());
					if("No".equalsIgnoreCase(recordsResult.getUPatchServer())) {
						firewallExceptionObject.setNotPatchedJustification(recordsResult.getUJustification());
					}
				}
				
				if (recordsResult.getUAppPswd() != null && !recordsResult.getUAppPswd().isEmpty()) {
					firewallExceptionObject.setDefaultPasswdChanged(recordsResult.getUAppPswd());
					logger.info("recordsResult.getUAppPswd(): " + recordsResult.getUAppPswd());
					if("No".equalsIgnoreCase(recordsResult.getUAppPswd())) {
						firewallExceptionObject.setPasswdNotChangedJustification(recordsResult.getUJustification1());;
					}
				}
				
				if (recordsResult.getUMgmtConsol() != null && !recordsResult.getUMgmtConsol().isEmpty()) {
					firewallExceptionObject.setAppConsoleACLed(recordsResult.getUMgmtConsol());
					logger.info("recordsResult.getUMgmtConsol(): " + recordsResult.getUMgmtConsol());
					if("No".equalsIgnoreCase(recordsResult.getUMgmtConsol())) {
						firewallExceptionObject.setNotACLedJustification(recordsResult.getUJustification2());;
					}
				}
				
				if (recordsResult.getUGuidelines() != null && !recordsResult.getUGuidelines().isEmpty()) {
					firewallExceptionObject.setHardened(recordsResult.getUGuidelines());
					logger.info("recordsResult.getUGuidelines(): " + recordsResult.getUGuidelines());
					if("No".equalsIgnoreCase(recordsResult.getUGuidelines())) {
						firewallExceptionObject.setNotHardenedJustification(recordsResult.getUJustification3());;
					}
				}
				
				
				if (recordsResult.getUPlan() != null && !recordsResult.getUPlan().isEmpty()) {
					firewallExceptionObject.setPatchingPlan(recordsResult.getUPlan());
					logger.info("recordsResult.getUPlan(): " + recordsResult.getUPlan());
				}
	
				if (recordsResult.getUComp1() != null && !recordsResult.getUComp1().isEmpty()) {
					firewallExceptionObject.addCompliance(recordsResult.getUComp1());
					logger.info("recordsResult.getUComp1(): " + recordsResult.getUComp1());
				}
				if (recordsResult.getUComp2() != null && !recordsResult.getUComp2().isEmpty()) {
					firewallExceptionObject.addCompliance(recordsResult.getUComp2());
					logger.info("recordsResult.getUComp2(): " + recordsResult.getUComp2());
				}
				if (recordsResult.getUComp3() != null && !recordsResult.getUComp3().isEmpty()) {
					firewallExceptionObject.addCompliance(recordsResult.getUComp3());
					logger.info("recordsResult.getUComp3(): " + recordsResult.getUComp3());
				}
				if (recordsResult.getUComp4() != null && !recordsResult.getUComp4().isEmpty()) {
					firewallExceptionObject.addCompliance(recordsResult.getUComp4());
					logger.info("recordsResult.getUComp4(): " + recordsResult.getUComp4());
				}
				if (recordsResult.getUComp5() != null && !recordsResult.getUComp5().isEmpty()) {
					firewallExceptionObject.addCompliance(recordsResult.getUComp5());
					logger.info("recordsResult.getUComp5(): " + recordsResult.getUComp5());
				}
				if (recordsResult.getUComp6() != null && !recordsResult.getUComp6().isEmpty()) {
					firewallExceptionObject.addCompliance(recordsResult.getUComp6());
					logger.info("recordsResult.getUComp6(): " + recordsResult.getUComp6());
				}

				if (recordsResult.getUOtherCompliance() != null && !recordsResult.getUOtherCompliance().isEmpty()) {
					firewallExceptionObject.setOtherCompliance(recordsResult.getUOtherCompliance());
					logger.info("recordsResult.getUOtherCompliance(): " + recordsResult.getUOtherCompliance());
				}
				if (recordsResult.getUSensitivity() != null && !recordsResult.getUSensitivity().isEmpty()) {
					firewallExceptionObject.setSensitiveDataDesc(recordsResult.getUSensitivity());
					logger.info("recordsResult.getUSensitivity(): " + recordsResult.getUSensitivity());
				}
				if (recordsResult.getUFirewallRules() != null && !recordsResult.getUFirewallRules().isEmpty()) {
					firewallExceptionObject.setLocalFirewallRules(recordsResult.getUFirewallRules());
					logger.info("recordsResult.getUFirewallRules(): " + recordsResult.getUFirewallRules());
				}
				if (recordsResult.getUSpecificDate() != null && !recordsResult.getUSpecificDate().isEmpty()) {
	
					Date openEaiDate = CalendarUtil.constructOpenEAIDate(recordsResult.getUSpecificDate());
	
					//System.out.println("recordsResult.getUSpecificDate(): " + recordsResult.getUSpecificDate());
					firewallExceptionObject.setValidUntilDate(openEaiDate);
					logger.info("recordsResult.getUSubnet(): " + recordsResult.getUSubnet());
				}
				if (recordsResult.getUSubnet() != null && !recordsResult.getUSubnet().isEmpty()) {
					firewallExceptionObject.setDefaultDenyZone(recordsResult.getUSubnet());
					logger.info("recordsResult.getUSubnet(): " + recordsResult.getUSubnet());
				}
				if (recordsResult.getUTag() != null && !recordsResult.getUTag().isEmpty()) {
					StringTokenizer tokenizer = new StringTokenizer(recordsResult.getUTag(), ",");
					while (tokenizer.hasMoreTokens()) {
						firewallExceptionObject.addTag(tokenizer.nextToken());
					}
					logger.info("recordsResult.getUTag(): " + recordsResult.getUTag());
				}
				
				if(recordsResult.getUTraverseVpn()!=null && !recordsResult.getUTraverseVpn().isEmpty()) {
					firewallExceptionObject.setWillTraverseVPN(recordsResult.getUTraverseVpn());
					logger.info("recordsResult.getUTraverseVpn():"+recordsResult.getUTraverseVpn());
					if ("Yes".equalsIgnoreCase(recordsResult.getUTraverseVpn())) {
						firewallExceptionObject.setVPNName(recordsResult.getUVpnName());
						logger.info("recordsResult.getUVpnName():"+recordsResult.getUVpnName());
					}
				}
				
				if (recordsResult.getUAmazonVpc()!=null && !recordsResult.getUAmazonVpc().isEmpty()) {
					firewallExceptionObject.setAccessAwsVPC(recordsResult.getUAmazonVpc());
				}
				
				addRequests.add(firewallExceptionObject);
				
			} else {
				logger.warn("Querying for a FirewallExceptionAddRequest returned a non-add request.  Returned requestType is " + requestType);
			}
		}
		
		return addRequests;
	}

	@Override
	public List<XmlEnterpriseObject> query(FirewallExceptionAddRequestQuerySpecification querySpec)
			throws ProviderException {

		logger.info(LOGTAG + "In query(). Ready to execute request.");
		GetRecordsResponseDocument getRecordsResponseDocument = GetRecordsResponseDocument.Factory.newInstance();
		try {
			getRecordsResponseDocument = FirewallExceptionRequestClient.doGetRecordsSearch(this.getFirewallExceptionService(),
					querySpec);
		} catch (RemoteException re) {
			String errMsg = "An error occurred calling ServiceNow from query(), please check the log for more detail";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}

		List<XmlEnterpriseObject> responseMoas = null;
		if (getRecordsResponseDocument != null && getRecordsResponseDocument.getGetRecordsResponse() != null
				&& getRecordsResponseDocument.getGetRecordsResponse().getGetRecordsResultArray().length > 0) {

			GetRecordsResponse resultData = getRecordsResponseDocument.getGetRecordsResponse();
			try {
				responseMoas = getFirewallExceptionResponse(resultData);
			} catch (EnterpriseConfigurationObjectException ecoe) {
				String errMsg = "An error occurred retrieving an object from ServiceNow client during doFirewallExceptionGrant()";
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, ecoe);
			} catch (EnterpriseFieldException efe) {
				String errMsg = "An error occurred retrieving an object from ServiceNow client during doFirewallExceptionGrant()";
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, efe);
			}

		}
		return responseMoas;
	}

	@Override
	public void delete(FirewallExceptionAddRequest aRequest) throws ProviderException {
		logger.info(LOGTAG + "Ready to execute FirewallExceptionAddRequest.Delete-Request.");
		
		logger.info(LOGTAG + "Ready to execute FirewallExceptionAddRequest.Delete-Request.");
		logger.info(LOGTAG + "Check to see whether the request exists in ServiceNow...");
		FirewallExceptionAddRequest current = this.query(aRequest.getRequestItemNumber());	
		logger.info("this corresponding request in ServiceNow is: "+ current);
		
		if (current != null) {
			if (ServiceNowValues.OPEN_STATE.getName().equals(current.getRequestItemState())) {

				try {
					// the delete web service call will do cancel instead
					FirewallExceptionRequestClient.doFirewallExceptionDelete(this.getFirewallExceptionService(), current.getSystemId());

				} catch (RemoteException re) {
					String errMsg = "An error occurred calling ServiceNow to delete a FirewallExceptionAddRequest, please check the log for more details";
					logger.error((LOGTAG + errMsg));
					throw new ProviderException(errMsg, re);
				}

			} else if (ServiceNowValues.CLOSED_CANCEL_STATE.getName().equals(current.getRequestState())) {
				logger.info("The state of this request is "+current.getRequestState());
				throw new ProviderException("The request has been cancelled and cannot be deleted.");
			} else {
				logger.info("The state of this request is "+current.getRequestState());
				throw new ProviderException("The request has been worked on and cannot be deleted.");
			}
		
		} else {
			logger.info("The request does not exist in ServiceNow");
			throw new ProviderException("The request does not exist in ServiceNow.");
		}

		return;
	}

	@Override
	public FirewallExceptionAddRequest generate(FirewallExceptionAddRequestRequisition requisition) throws ProviderException {
		logger.info(LOGTAG + "Ready to execute FirewallExceptionAddRequest.Generate-Request.");

		String itemNumber = null;

		try {
			InsertResponseDocument insertResponseDocument = null;
			insertResponseDocument = FirewallExceptionRequestClient.doFirewallExceptionCreate(this.getFirewallExceptionService(),
					requisition);
			if ((insertResponseDocument != null && insertResponseDocument.getInsertResponse() != null
					&& insertResponseDocument.getInsertResponse().getStatusMessage() != null
					&& !insertResponseDocument.getInsertResponse().getStatusMessage().isEmpty())) {

				String[] statusMessage = StringUtil
						.splitString(insertResponseDocument.getInsertResponse().getStatusMessage());
				itemNumber = statusMessage[0];

			}
		} catch (RemoteException re) {
			String errMsg = "An error occurred calling ServiceNow from generate(), please check the log for more detail";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}
		
		FirewallExceptionAddRequest addRequest = null;
		if (itemNumber != null && itemNumber.length() > 0) {
			logger.info("Item number from FirewallExceptionRemoveRequest.Generate-Request " + itemNumber);
			addRequest = this.query(itemNumber);
		} 
		
		return addRequest;
	}

	@Override
	public FirewallExceptionAddRequest query(String itemNumber) throws ProviderException {
		
		FirewallExceptionAddRequest addRequest = null;
		
		logger.info(LOGTAG + "Ready to execute FirewallExceptionAddRequest.Query-Request. RITM: "+itemNumber);
		
		if (itemNumber != null && itemNumber.length() > 0) {
			
			FirewallExceptionAddRequestQuerySpecification querySpec = new FirewallExceptionAddRequestQuerySpecification();
			try {
				querySpec.setRequestItemNumber(itemNumber);
			} catch (EnterpriseFieldException e) {
				String errMsg = "An error occurred when setRequestItemNumber(): "+itemNumber;
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, e);
			}
			
			List<XmlEnterpriseObject> results = this.query(querySpec);
			
			if (results.size()>0) {
				addRequest = (FirewallExceptionAddRequest) results.get(0);
			}
			
		} 
		
		return addRequest;
	}
	

}