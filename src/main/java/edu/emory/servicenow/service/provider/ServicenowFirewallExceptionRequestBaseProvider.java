package edu.emory.servicenow.service.provider;

import java.rmi.RemoteException;
import java.util.Properties;

import org.apache.axis2.Constants;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.impl.httpclient3.HttpTransportPropertiesImpl;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.PropertyConfig;
import org.openeai.moa.XmlEnterpriseObjectImpl;

import edu.emory.axis2.servicenow.firewallexception.ServiceNow_u_esb_firewall_request_crudStub;
import edu.emory.servicenow.service.util.PropertyNames;

/**
 * Provider implementation for ServicenowFirewallExceptionProvider
 * 
 * @author 
 * @nchen3
 */
public class ServicenowFirewallExceptionRequestBaseProvider extends OpenEaiObject {
	private static Logger logger = Logger.getLogger(ServicenowFirewallExceptionRequestBaseProvider.class);
	private AppConfig appConfig;
	private String serviceUserId = "";
	private ServiceNow_u_esb_firewall_request_crudStub firewallExceptionService = null;

	private String servicePassword = "";
	private static long maxWaitTime = 30000;
	private static long waitInterval = 8000;
	private String firewallExceptionServiceEndpoint = "";
	private int connectionTimeout = 12000;
	private String LOGTAG = "[ServicenowFirewallExceptionBaseProvider] ";

	public void init(AppConfig aConfig) throws ProviderException {

		logger.info(LOGTAG + "Initializing...");
		setAppConfig(aConfig);
		// Get the provider properties
		PropertyConfig pConfig = new PropertyConfig();
		try {
			pConfig = (PropertyConfig) aConfig.getObject("FirewallExceptionRequestProviderProperties");
			Properties props = pConfig.getProperties();
			setProperties(props);
		} catch (EnterpriseConfigurationObjectException eoce) {
			String errMsg = "Error retrieving a PropertyConfig object from AppConfig: The exception is:";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, eoce);
		}

		if (getProperties() != null && !getProperties().isEmpty()) {
			if (getProperties().getProperty(PropertyNames.FIREWALL_WEB_SERVICE_ENDPOINT.toString()) != null) {
				setFirewallExceptionServiceEndpoint(
						getProperties().getProperty(PropertyNames.FIREWALL_WEB_SERVICE_ENDPOINT.toString()));
				logger.info(LOGTAG + PropertyNames.FIREWALL_WEB_SERVICE_ENDPOINT.toString() + " = "
						+ firewallExceptionServiceEndpoint);
			} else {
				String errMsg = " REQUIRED Property " + PropertyNames.FIREWALL_WEB_SERVICE_ENDPOINT.toString()
						+ " not defined and is expected to be passed in via the appConfig ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			// Set the user id to log into ServiceNow with from config. If
			// it's not there, throw an error.
			if (getProperties().getProperty(PropertyNames.WEB_SERVICE_USER_ID.toString()) != null) {
				setServiceUserId(getProperties().getProperty(PropertyNames.WEB_SERVICE_USER_ID.toString()));
				logger.info(LOGTAG + PropertyNames.WEB_SERVICE_USER_ID.toString() + " = " + serviceUserId);
			} else {
				String errMsg = " REQUIRED Property " + PropertyNames.WEB_SERVICE_USER_ID.toString()
						+ " not defined and is expected to be passed in via the appConfig ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			// Set the password to log into Servicenow with from config. If
			// it's not there, throw an error.
			if (getProperties().getProperty(PropertyNames.WEB_SERVICE_PASSWORD.toString()) != null) {
				setServicePassword(getProperties().getProperty(PropertyNames.WEB_SERVICE_PASSWORD.toString()));
				logger.info(LOGTAG + PropertyNames.WEB_SERVICE_PASSWORD.toString() + " = " + servicePassword);
			} else {
				String errMsg = " REQUIRED Property " + PropertyNames.WEB_SERVICE_PASSWORD.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.CONNECTION_TIMEOUT.toString()) != null) {
				setConnectionTimeout(
						Integer.parseInt(getProperties().getProperty(PropertyNames.CONNECTION_TIMEOUT.toString())));
				logger.info(LOGTAG + PropertyNames.CONNECTION_TIMEOUT.toString() + " = " + connectionTimeout);
			} else {
				logger.info(
						LOGTAG + "OPTIONAL Property " + PropertyNames.CONNECTION_TIMEOUT.toString() + " not defined");
			}

		} else {
			String errMsg = "REQUIRED Properties have not been defined. Please define required properties within AppConfig.";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg);
		}

		try {
			firewallExceptionService = getFirewallExceptionStub(getServiceUserId(), getServicePassword(),
					getFirewallExceptionServiceEndpoint(), getConnectionTimeout());

		} catch (RemoteException re) {
			String errMsg = "The FirewallException Service Stub is not started successfully. The error message is: ";
			logger.error(LOGTAG + errMsg + re.getMessage());
			throw new ProviderException(errMsg, re);
		}
	}

	/**
	 * Converts oEAI object type to XmlEnterpriseObject
	 * 
	 * @param aClass
	 * @return
	 * @throws EnterpriseConfigurationObjectException
	 */
	@SuppressWarnings("unchecked")
	protected <T extends XmlEnterpriseObjectImpl> T getObject(Class<T> aClass)
			throws EnterpriseConfigurationObjectException {
		return (T) getAppConfig().getObjectByType(aClass.getName());
	}

	/**
	 * Creates a FirewallException Service stub for the remote object acts as a
	 * client's local proxy, so the caller can invoke a method on the client
	 * that will carry out the method call for the remote object. A
	 * multithreaded client will be set up in order to enable concurrent access.
	 * 
	 * @param userid
	 * @param password
	 * @param endpoint
	 * @param timeout
	 * @return
	 * @throws RemoteException
	 */
	private ServiceNow_u_esb_firewall_request_crudStub getFirewallExceptionStub(String userid, String password,
			String endpoint, int timeout) throws RemoteException {

		ServiceNow_u_esb_firewall_request_crudStub service = null;
		EndpointReference targetEPR = new EndpointReference(endpoint);
		service = new ServiceNow_u_esb_firewall_request_crudStub();
		HttpTransportPropertiesImpl.Authenticator auth = new HttpTransportPropertiesImpl.Authenticator();
		auth.setUsername(userid);
		auth.setPassword(password);
		auth.setPreemptiveAuthentication(true);

		MultiThreadedHttpConnectionManager conmgr = new MultiThreadedHttpConnectionManager();
		Options options = new Options();
		options.setProperty(HTTPConstants.AUTHENTICATE, auth);

		// Set up concurrent access by creating a pooling manager to manage
		// connections.
		conmgr.getParams().setDefaultMaxConnectionsPerHost(10);
		HttpClient client = new HttpClient(conmgr);
		options.setProperty(HTTPConstants.CACHED_HTTP_CLIENT, client);
		options.setTo(targetEPR);
		options.setTransportInProtocol(Constants.TRANSPORT_HTTP);
		options.setTimeOutInMilliSeconds(timeout);
		service._getServiceClient().setOptions(options);

		return service;
	}
	
	
	public AppConfig getAppConfig() {
		return appConfig;
	}

	public void setAppConfig(AppConfig appConfig) {
		this.appConfig = appConfig;
	}

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public String getServiceUserId() {
		return serviceUserId;
	}

	public void setServiceUserId(String serviceUserId) {
		this.serviceUserId = serviceUserId;
	}

	public String getServicePassword() {
		return servicePassword;
	}

	public void setServicePassword(String servicePassword) {
		this.servicePassword = servicePassword;
	}

	public static long getWaitInterval() {
		return waitInterval;
	}

	public static void setWaitInterval(long waitInterval) {
		ServicenowFirewallExceptionRequestBaseProvider.waitInterval = waitInterval;
	}

	public static long getMaxWaitTime() {
		return maxWaitTime;
	}

	public static void setMaxWaitTime(long maxWaitTime) {
		ServicenowFirewallExceptionRequestBaseProvider.maxWaitTime = maxWaitTime;
	}

	public String getFirewallExceptionServiceEndpoint() {
		return firewallExceptionServiceEndpoint;
	}

	public void setFirewallExceptionServiceEndpoint(String firewallExceptionServiceEndpoint) {
		this.firewallExceptionServiceEndpoint = firewallExceptionServiceEndpoint;
	}

	public ServiceNow_u_esb_firewall_request_crudStub getFirewallExceptionService() {
		return firewallExceptionService;
	}
	
}