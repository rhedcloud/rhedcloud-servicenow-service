package edu.emory.servicenow.service.provider;

public class ProviderException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProviderException() {
	}

	public ProviderException(String msg) {
		super(msg);
	}

	public ProviderException(String msg, Throwable e) {
		super(msg, e);
	}
}

