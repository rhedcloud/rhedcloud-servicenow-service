package edu.emory.servicenow.service.provider;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.axis2.Constants;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.impl.httpclient4.HttpTransportPropertiesImpl;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObjectImpl;

import com.service_now.moa.jmsobjects.servicedesk.v2_0.Incident;
import com.service_now.moa.objects.resources.v2_0.IncidentQuerySpecification;
import com.service_now.moa.objects.resources.v2_0.IncidentRequisition;
//import edu.emory.axis2.servicenow.incident.ServiceNow_incidentStub;
import com.service_now.www.ServiceNow_incidentStub;
import com.service_now.www.incident.GetRecordsResponseDocument;
import com.service_now.www.incident.GetRecordsResponseDocument.GetRecordsResponse;
import com.service_now.www.incident.GetRecordsResponseDocument.GetRecordsResponse.GetRecordsResult;
import com.service_now.www.incident.InsertResponseDocument;

import edu.emory.servicenow.service.client.IncidentClient;
import edu.emory.servicenow.service.util.PropertyNames;

/**
 * Provider implementation for IncidentProvider
 * 
 * @author RXING2
 *
 */
public class ServicenowIncidentProvider extends OpenEaiObject implements IncidentProvider {
	private static Logger logger = Logger.getLogger(ServicenowIncidentProvider.class);
	private AppConfig appConfig;
	private String serviceUserId = "";
	private ServiceNow_incidentStub incidentService = null;
	private String servicePassword = "";
	private static long maxWaitTime = 30000;
	private String incidentServiceEndpoint = "";
	private int connectionTimeout = 12000;
	private String LOGTAG = "[ServicenowIncidentServiceProvider] ";

	/**
	 * @throws EnterpriseConfigurationObjectException
	 * @see RoleAssignmentProvider.java
	 */
	public void init(AppConfig aConfig) throws ProviderException {

		logger.info(LOGTAG + "Initializing...");
		setAppConfig(aConfig);
		// Get the provider properties
		PropertyConfig pConfig = new PropertyConfig();
		try {
			pConfig = (PropertyConfig) aConfig.getObject("IncidentProviderProperties");
			Properties props = pConfig.getProperties();
			setProperties(props);
		} catch (EnterpriseConfigurationObjectException eoce) {
			String errMsg = "Error retrieving a PropertyConfig object from AppConfig: The exception is:";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, eoce);
		}

		if (getProperties() != null && !getProperties().isEmpty()) {
			if (getProperties().getProperty(PropertyNames.INCIDENT_WEB_SERVICE_ENDPOINT.toString()) != null) {
				setIncidentServiceEndpoint(getProperties().getProperty(PropertyNames.INCIDENT_WEB_SERVICE_ENDPOINT.toString()));
				logger.info(LOGTAG + PropertyNames.INCIDENT_WEB_SERVICE_ENDPOINT.toString() + " = " + incidentServiceEndpoint);
			} else {
				String errMsg = " REQUIRED Property " + PropertyNames.INCIDENT_WEB_SERVICE_ENDPOINT.toString()
						+ " not defined and is expected to be passed in via the appConfig ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}
			
			if (getProperties().getProperty(PropertyNames.WEB_SERVICE_USER_ID.toString()) != null) {
				setServiceUserId(getProperties().getProperty(PropertyNames.WEB_SERVICE_USER_ID.toString()));
				logger.info(LOGTAG + PropertyNames.WEB_SERVICE_USER_ID.toString() + " = " + serviceUserId);
			} else {
				String errMsg = " REQUIRED Property " + PropertyNames.WEB_SERVICE_USER_ID.toString()
						+ " not defined and is expected to be passed in via the appConfig ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			// Set the password to log into IDM with from config. If
			// it's not there, throw an error.
			if (getProperties().getProperty(PropertyNames.WEB_SERVICE_PASSWORD.toString()) != null) {
				setServicePassword(getProperties().getProperty(PropertyNames.WEB_SERVICE_PASSWORD.toString()));
				logger.info(LOGTAG + PropertyNames.WEB_SERVICE_PASSWORD.toString() + " = " + servicePassword);
			} else {
				String errMsg = " REQUIRED Property " + PropertyNames.WEB_SERVICE_PASSWORD.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.CONNECTION_TIMEOUT.toString()) != null) {
				setConnectionTimeout(
						Integer.parseInt(getProperties().getProperty(PropertyNames.CONNECTION_TIMEOUT.toString())));
				logger.info(LOGTAG + PropertyNames.CONNECTION_TIMEOUT.toString() + " = " + connectionTimeout);
			} else {
				logger.info(
						LOGTAG + "OPTIONAL Property " + PropertyNames.CONNECTION_TIMEOUT.toString() + " not defined");
			}
		} else {
			String errMsg = "REQUIRED Properties have not been defined. Please define required properties within AppConfig.";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg);
		}
		
		try {
			incidentService = getIncidentStub(getServiceUserId(), getServicePassword(),
					getIncidentServiceEndpoint(), getConnectionTimeout());
		} catch (RemoteException re) {
			String errMsg = "The Incident Service stub is not started successfully. The error message is: ";
			logger.error(LOGTAG + errMsg + re.getMessage());
			throw new ProviderException(errMsg, re);
		}
	}

	@Override
	public Incident query(String itemNumber) throws ProviderException {

		logger.info(LOGTAG + "In query(). Ready to execute request.");	
		GetRecordsResponseDocument getRecordsResponseDocument = GetRecordsResponseDocument.Factory.newInstance();
		try {
			getRecordsResponseDocument = IncidentClient.doGetRecordsSearch(incidentService,
					itemNumber);
		} catch (RemoteException re) {
			String errMsg = "An error occurred calling ServiceNow from query(), please check the log for more detail";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}

		Incident responseMoa = null;
		if (getRecordsResponseDocument != null && getRecordsResponseDocument.getGetRecordsResponse() != null
				&& getRecordsResponseDocument.getGetRecordsResponse().getGetRecordsResultArray().length > 0) {
	
			try {
				responseMoa = this.getObject(Incident.class);
			} catch (EnterpriseConfigurationObjectException ecoe) {
				String errMsg = "An error occurred retrieving an object from AppConfig. The exception is: "
						+ ecoe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, ecoe);
			}
			
			List<GetRecordsResult> recordsResult = Arrays
					.asList(getRecordsResponseDocument.getGetRecordsResponse().getGetRecordsResultArray()).stream()
					.limit(1).collect(Collectors.toList());
			try {
				responseMoa = this.getObject(Incident.class);
			} catch (EnterpriseConfigurationObjectException ecoe) {
				String errMsg = "An error occurred retrieving an object from AppConfig. The exception is: "
						+ ecoe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, ecoe);
			}

			try {
				GetRecordsResult aRecord = recordsResult.get(0);
				this.populateIncidentObject(aRecord, responseMoa);
				
//				if (recordsResult.get(0).getSysId() != null && !recordsResult.get(0).getSysId().isEmpty()) {
//					//System.out.println("systemId: "+recordsResult.get(0).getSysId());
//					responseMoa.setSystemId(recordsResult.get(0).getSysId());
//				}
//				
//				if (recordsResult.get(0).getNumber() != null && !recordsResult.get(0).getNumber().isEmpty()) {
//					responseMoa.setNumber(recordsResult.get(0).getNumber());
//				}
//				
//				if (recordsResult.get(0).getUNocAlarmId() != null
//						&& !recordsResult.get(0).getUNocAlarmId().isEmpty()) {
//					responseMoa.setNocAlarmId(recordsResult.get(0).getUNocAlarmId());
//				}
//				
//				if (recordsResult.get(0).getCategory() != null && !recordsResult.get(0).getCategory().isEmpty()) {
//					responseMoa.setCategory(recordsResult.get(0).getCategory());
//				}
//				
//				if (recordsResult.get(0).getCallerId() != null
//						&& !recordsResult.get(0).getCallerId().isEmpty()) {
//					responseMoa.setCallerId(recordsResult.get(0).getCallerId());
//				}
//				
//				if (recordsResult.get(0).getShortDescription() != null
//						&& !recordsResult.get(0).getShortDescription().isEmpty()) {
//					//System.out.println("recordsResult.get(0).getShortDescription(): "+recordsResult.get(0).getShortDescription());
//					responseMoa.setShortDescription(recordsResult.get(0).getShortDescription());
//				}
//				if (recordsResult.get(0).getCmdbCi() != null && !recordsResult.get(0).getCmdbCi().isEmpty()) {
//					responseMoa.setCmdbCi(recordsResult.get(0).getCmdbCi());
//				}
//				if (recordsResult.get(0).getSubcategory() != null && !recordsResult.get(0).getSubcategory().isEmpty()) {
//					responseMoa.setSubCategory(recordsResult.get(0).getSubcategory());
//				}
//				if (recordsResult.get(0).getDescription() != null
//						&& !recordsResult.get(0).getDescription().isEmpty()) {
//					//System.out.println("recordsResult.get(0).getDescription(): "+recordsResult.get(0).getDescription());
//					responseMoa.setDescription(recordsResult.get(0).getDescription());
//				}
//				if (recordsResult.get(0).getBusinessService() != null
//						&& !recordsResult.get(0).getBusinessService().isEmpty()) {
//					System.out.println("recordsResult.get(0).getBusinessService(): "+recordsResult.get(0).getBusinessService());
//					responseMoa.setBusinessService(recordsResult.get(0).getBusinessService());
//				}	
//				
////				if (String.valueOf(recordsResult.get(0).getUrgency().intValue()) != null){
////					System.out.println("converted successfully..."+String.valueOf(recordsResult.get(0).getUrgency().intValue()));
////					System.out.println("value: "+recordsResult.get(0).getUrgency());
////					responseMoa.setUrgency(String.valueOf(recordsResult.get(0).getUrgency().intValue()));
////				}
//				responseMoa.setUrgency(recordsResult.get(0).getUrgency());
//				responseMoa.setAssignmentGroup(recordsResult.get(0).getAssignmentGroup());
//				responseMoa.setRecordType(recordsResult.get(0).getURecordType());
//				responseMoa.setContactType(recordsResult.get(0).getContactType());
//			
//				if (recordsResult.get(0).getSysId() != null && recordsResult.get(0).getSysId().length() > 0) {
//					responseMoa.setSystemId(recordsResult.get(0).getSysId());
//				}
				
			} catch (EnterpriseFieldException efe) {
				String errMsg = "An error occurred setting field for the " + "Incident.  The exception is: "
						+ efe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}
		}
	
	
		return responseMoa;
	}

	@Override
	public Incident generate(IncidentRequisition incidentRequisition)
			throws ProviderException {

		logger.info(LOGTAG + "Ready to execute Incident.Generate-Request.");
		InsertResponseDocument insertResponseDocument = null;
		String itemNumber = null;
		Incident incident = null;
		try {
			insertResponseDocument = IncidentClient.doIncidentCreate(incidentService,
					incidentRequisition);
			if ((insertResponseDocument != null && insertResponseDocument.getInsertResponse() != null
					&& insertResponseDocument.getInsertResponse().getNumber() != null
					&& !insertResponseDocument.getInsertResponse().getNumber().isEmpty())) {

				itemNumber = insertResponseDocument.getInsertResponse().getNumber();
				logger.info(LOGTAG + "itemNumber: "+itemNumber);
			}
		}
		catch (RemoteException re) {
			System.out.println("get message: "+re.getStackTrace());
			if ( re.getCause() != null && re.getCause().getLocalizedMessage() !=null ){
				System.out.println("get message1: "+re.getCause().getMessage());
			}
			String errMsg = "An error occurred calling ServiceNow from generate(), please check the log for more detail";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}
		if (itemNumber != null && itemNumber.length() > 0) {
			incident = this.query(itemNumber);
		}
		return incident;
	}

	@Override
	public void delete(String id) throws ProviderException {
		
		logger.info(LOGTAG + "Ready to execute Incident.Delete-Request. id: "+id);
		try {
			IncidentClient.doIncidentDelete(incidentService, id);
		} catch (RemoteException re) {
			String errMsg = "An error occurred calling ServiceNow from delete(), please check the log for more detail";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}
	}

	@Override
	public void update(Incident incident) throws ProviderException {

		logger.info(LOGTAG + "Ready to execute Generate-Request.");
		try {
			IncidentClient.doIncidentUpdate(incidentService, incident);
		} catch (RemoteException re) {
			String errMsg = "An error occurred calling ServiceNow from update(): " + re.getMessage();
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}
	}

	/**
	 * Converts oEAI object type to XmlEnterpriseObject
	 * 
	 * @param aClass
	 * @return
	 * @throws EnterpriseConfigurationObjectException
	 */
	@SuppressWarnings("unchecked")
	private <T extends XmlEnterpriseObjectImpl> T getObject(Class<T> aClass)
			throws EnterpriseConfigurationObjectException {
		return (T) getAppConfig().getObjectByType(aClass.getName());
	}

	/**
	 * Creates a Role Service stub for the remote object acts as a client's
	 * local proxy, so the caller can invoke a method on the client that will
	 * carry out the method call for the remote object. A multithreaded client
	 * will be set up in order to enable concurrent access.
	 * 
	 * @param userid
	 * @param password
	 * @param endpoint
	 * @param timeout
	 * @return
	 * @throws RemoteException
	 */
	private ServiceNow_incidentStub getIncidentStub(String userid, String password,
			String endpoint, int timeout) throws RemoteException {
		
		EndpointReference targetEPR = new EndpointReference(endpoint);
		incidentService = new ServiceNow_incidentStub();
		HttpTransportPropertiesImpl.Authenticator auth = new HttpTransportPropertiesImpl.Authenticator();
		auth.setUsername(userid);
		auth.setPassword(password);
		auth.setPreemptiveAuthentication(true);

		Options options = new Options();
		options.setProperty(HTTPConstants.AUTHENTICATE, auth);
		// options.setProperty(HTTPConstants.REUSE_HTTP_CLIENT, "true");
		// Set up concurrent access by creating a pooling manager to manage
		// connections.
		MultiThreadedHttpConnectionManager conmgr = new MultiThreadedHttpConnectionManager();
		conmgr.getParams().setDefaultMaxConnectionsPerHost(10);
		HttpClient client = new HttpClient(conmgr);
		options.setProperty(HTTPConstants.CACHED_HTTP_CLIENT, client);
		options.setTo(targetEPR);
		options.setTransportInProtocol(Constants.TRANSPORT_HTTP);
		options.setTimeOutInMilliSeconds(timeout);
		incidentService._getServiceClient().setOptions(options);

		return incidentService;

	}

	public AppConfig getAppConfig() {
		return appConfig;
	}

	public void setAppConfig(AppConfig appConfig) {
		this.appConfig = appConfig;
	}

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public String getServiceUserId() {
		return serviceUserId;
	}

	public void setServiceUserId(String serviceUserId) {
		this.serviceUserId = serviceUserId;
	}

	public String getServicePassword() {
		return servicePassword;
	}

	public void setServicePassword(String servicePassword) {
		this.servicePassword = servicePassword;
	}

	private static long waitInterval = 8000;

	public static long getWaitInterval() {
		return waitInterval;
	}

	public static void setWaitInterval(long waitInterval) {
		ServicenowIncidentProvider.waitInterval = waitInterval;
	}

	public static long getMaxWaitTime() {
		return maxWaitTime;
	}

	public static void setMaxWaitTime(long maxWaitTime) {
		ServicenowIncidentProvider.maxWaitTime = maxWaitTime;
	}
	
	public String getIncidentServiceEndpoint() {
		return incidentServiceEndpoint;
	}
	
	public void setIncidentServiceEndpoint(String incidentServiceEndpoint) {
		this.incidentServiceEndpoint = incidentServiceEndpoint;
	}

	@Override
	public List<XmlEnterpriseObject> query(IncidentQuerySpecification queryData) throws ProviderException {
		
		logger.info(LOGTAG + "In query(). Ready to execute request.");	
		GetRecordsResponseDocument getRecordsResponseDocument = GetRecordsResponseDocument.Factory.newInstance();
		try {
			getRecordsResponseDocument = IncidentClient.doGetRecordsSearch(incidentService,
					queryData);
		} catch (RemoteException re) {
			String errMsg = "An error occurred calling ServiceNow from query(), please check the log for more detail";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}

		List<XmlEnterpriseObject> responseMoas = null;
		if (getRecordsResponseDocument != null && getRecordsResponseDocument.getGetRecordsResponse() != null
				&& getRecordsResponseDocument.getGetRecordsResponse().getGetRecordsResultArray().length > 0) {

			GetRecordsResponse resultData = getRecordsResponseDocument.getGetRecordsResponse();
			try {
				responseMoas = getIncidentResponse(resultData);
			} catch (EnterpriseConfigurationObjectException ecoe) {
				String errMsg = "An error occurred retrieving an object from ServiceNow client during doFirewallExceptionGrant()";
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, ecoe);
			} catch (EnterpriseFieldException efe) {
				String errMsg = "An error occurred retrieving an object from ServiceNow client during doFirewallExceptionGrant()";
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, efe);
			}

		}
		return responseMoas;
		
	}
	
	private List<XmlEnterpriseObject> getIncidentResponse(GetRecordsResponse object)
			throws EnterpriseConfigurationObjectException, EnterpriseFieldException, ProviderException {

		List<XmlEnterpriseObject> incidentObjects = new ArrayList<XmlEnterpriseObject>();
		for (GetRecordsResult recordsResult : object.getGetRecordsResultArray()) {
		
			Incident incidentObject = this.getObject(Incident.class);
			populateIncidentObject(recordsResult, incidentObject);
			incidentObjects.add(incidentObject);
		}
		return incidentObjects;
	}

	private void populateIncidentObject(GetRecordsResult recordsResult, Incident incidentObject)
			throws EnterpriseFieldException {
		
		if (recordsResult.getSysId() != null && !recordsResult.getSysId().isEmpty()) {
			incidentObject.setSystemId(recordsResult.getSysId());
			logger.info("recordsResult..getSysId(): " + recordsResult.getSysId());
		}
		
		if (recordsResult.getNumber() != null && !recordsResult.getNumber().isEmpty()) {
			incidentObject.setNumber(recordsResult.getNumber());
		}		
		if (recordsResult.getUNocAlarmId() != null
				&& !recordsResult.getUNocAlarmId().isEmpty()) {
			incidentObject.setNocAlarmId(recordsResult.getUNocAlarmId());
		}		
		if (recordsResult.getCategory() != null && !recordsResult.getCategory().isEmpty()) {
			incidentObject.setCategory(recordsResult.getCategory());
		}		
		if (recordsResult.getCallerId() != null
				&& !recordsResult.getCallerId().isEmpty()) {
			incidentObject.setCallerId(recordsResult.getCallerId());
		}			
		if (recordsResult.getShortDescription() != null
				&& !recordsResult.getShortDescription().isEmpty()) {
			//System.out.println("recordsResult.get(0).getShortDescription(): "+recordsResult.get(0).getShortDescription());
			incidentObject.setShortDescription(recordsResult.getShortDescription());
		}
		if (recordsResult.getCmdbCi() != null && !recordsResult.getCmdbCi().isEmpty()) {
			incidentObject.setCmdbCi(recordsResult.getCmdbCi());
		}
		if (recordsResult.getSubcategory() != null && !recordsResult.getSubcategory().isEmpty()) {
			incidentObject.setSubCategory(recordsResult.getSubcategory());
		}
		if (recordsResult.getDescription() != null
				&& !recordsResult.getDescription().isEmpty()) {
			incidentObject.setDescription(recordsResult.getDescription());
		}
		if (recordsResult.getBusinessService() != null
				&& !recordsResult.getBusinessService().isEmpty()) {
			System.out.println("recordsResult.get(0).getBusinessService(): "+recordsResult.getBusinessService());
			incidentObject.setBusinessService(recordsResult.getBusinessService());
		}	
		
//			if (String.valueOf(recordsResult.getUrgency().intValue()) != null){
//				System.out.println("converted successfully..."+String.valueOf(recordsResult.getUrgency().intValue()));
//				System.out.println("value: "+recordsResult.getUrgency());
//				incidentObject.setUrgency(String.valueOf(recordsResult.getUrgency().intValue()));
//			}	
		
		incidentObject.setUrgency(recordsResult.getUrgency());
		incidentObject.setAssignmentGroup(recordsResult.getAssignmentGroup());
		incidentObject.setRecordType(recordsResult.getURecordType());
		incidentObject.setContactType(recordsResult.getContactType());
		incidentObject.setImpact(recordsResult.getImpact());
		incidentObject.setPriority(recordsResult.getPriority());
		incidentObject.setIncidentState(recordsResult.getIncidentState());
		
	}
}