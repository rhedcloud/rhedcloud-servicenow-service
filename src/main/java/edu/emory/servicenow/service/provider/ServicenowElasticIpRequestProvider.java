package edu.emory.servicenow.service.provider;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.axis2.Constants;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.impl.httpclient4.HttpTransportPropertiesImpl;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.log4j.Logger;

import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObjectImpl;

import com.service_now.moa.jmsobjects.customrequests.v1_0.ElasticIpRequest;
import com.service_now.moa.objects.resources.v1_0.ElasticIpRequestQuerySpecification;
import com.service_now.www.u_esb_elastic_ip_request_crud.DeleteRecordDocument;
import com.service_now.www.u_esb_elastic_ip_request_crud.DeleteRecordDocument.DeleteRecord;
import com.service_now.www.u_esb_elastic_ip_request_crud.GetRecordsResponseDocument;
import com.service_now.www.u_esb_elastic_ip_request_crud.GetRecordsResponseDocument.GetRecordsResponse;
import com.service_now.www.u_esb_elastic_ip_request_crud.GetRecordsResponseDocument.GetRecordsResponse.GetRecordsResult;
import com.service_now.www.u_esb_elastic_ip_request_crud.InsertResponseDocument;

import edu.emory.axis2.servicenow.elasticip.ServiceNow_u_esb_elastic_ip_request_crudStub;
import edu.emory.servicenow.service.client.ElasticIpRequestClient;
import edu.emory.servicenow.service.util.PropertyNames;
import edu.emory.servicenow.service.util.StringUtil;

/**
 * 
 * This class actually implements the provider class
 * ServicenowElasticIPProvider.
 *
 * @author RXING2
 */

public class ServicenowElasticIpRequestProvider extends OpenEaiObject implements ElasticIpRequestProvider {

	private static Logger logger = Logger.getLogger(ServicenowElasticIpRequestProvider.class);
	private AppConfig appConfig;
	private String serviceUserId = "";
	private ServiceNow_u_esb_elastic_ip_request_crudStub elasticIPService = null;
	private String servicePassword = "";
	// private String codeMapKey = "";
	private static long maxWaitTime = 30000;
	private static long waitInterval = 8000;
	private String elasticIPServiceEndpoint = "";
	private int connectionTimeout = 12000;
	private String LOGTAG = "[ServicenowElasticIPProvider] ";

	/**
	 * @throws EnterpriseConfigurationObjectException
	 * @see ServicenowElasticIpRequestProvider.java
	 */
	public void init(AppConfig aConfig) throws ProviderException {

		logger.info(LOGTAG + "Initializing...");
		setAppConfig(aConfig);
		// Get the provider properties
		PropertyConfig pConfig = new PropertyConfig();
		try {
			pConfig = (PropertyConfig) aConfig.getObject("ElasticIpRequestProviderProperties");
			Properties props = pConfig.getProperties();
			setProperties(props);
		} catch (EnterpriseConfigurationObjectException eoce) {
			String errMsg = "Error retrieving a PropertyConfig object from AppConfig: The exception is:";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, eoce);
		}

		if (getProperties() != null && !getProperties().isEmpty()) {

			if (getProperties().getProperty(PropertyNames.ELASTICIP_WEB_SERVICE_ENDPOINT.toString()) != null) {
				setElasticIPServiceEndpoint(
						getProperties().getProperty(PropertyNames.ELASTICIP_WEB_SERVICE_ENDPOINT.toString()));
				logger.info(LOGTAG + PropertyNames.ELASTICIP_WEB_SERVICE_ENDPOINT.toString() + " = "
						+ elasticIPServiceEndpoint);
			} else {
				String errMsg = " REQUIRED Property " + PropertyNames.ELASTICIP_WEB_SERVICE_ENDPOINT.toString()
						+ " not defined and is expected to be passed in via the appConfig ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			// Set the user id to log into Servicenowwith from config. If
			// it's not there, throw an error.
			if (getProperties().getProperty(PropertyNames.WEB_SERVICE_USER_ID.toString()) != null) {
				setServiceUserId(getProperties().getProperty(PropertyNames.WEB_SERVICE_USER_ID.toString()));
				logger.info(LOGTAG + PropertyNames.WEB_SERVICE_USER_ID.toString() + " = " + serviceUserId);
			} else {
				String errMsg = " REQUIRED Property " + PropertyNames.WEB_SERVICE_USER_ID.toString()
						+ " not defined and is expected to be passed in via the appConfig ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			// Set the password to log into Servicenow with from config. If
			// it's not there, throw an error.
			if (getProperties().getProperty(PropertyNames.WEB_SERVICE_PASSWORD.toString()) != null) {
				setServicePassword(getProperties().getProperty(PropertyNames.WEB_SERVICE_PASSWORD.toString()));
				logger.info(LOGTAG + PropertyNames.WEB_SERVICE_PASSWORD.toString() + " = " + servicePassword);
			} else {
				String errMsg = " REQUIRED Property " + PropertyNames.WEB_SERVICE_PASSWORD.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.CONNECTION_TIMEOUT.toString()) != null) {
				setConnectionTimeout(
						Integer.parseInt(getProperties().getProperty(PropertyNames.CONNECTION_TIMEOUT.toString())));
				logger.info(LOGTAG + PropertyNames.CONNECTION_TIMEOUT.toString() + " = " + connectionTimeout);
			} else {
				logger.info(
						LOGTAG + "OPTIONAL Property " + PropertyNames.CONNECTION_TIMEOUT.toString() + " not defined");
			}

		} else {
			String errMsg = "REQUIRED Properties have not been defined. Please define required properties within AppConfig.";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg);
		}

		try {
			elasticIPService = getElasticIPStub(getServiceUserId(), getServicePassword(), getElasticIPServiceEndpoint(),
					getConnectionTimeout());
		} catch (RemoteException re) {
			String errMsg = "The ElasticIp Service stub is not started successfully. The error message is: ";
			logger.error(LOGTAG + errMsg + re.getMessage());
			throw new ProviderException(errMsg, re);
		}
	}

	@Override
	public ElasticIpRequest query(ElasticIpRequest itemNumber) throws ProviderException {

		logger.info(LOGTAG + "In query(). Ready to execute request.");
		GetRecordsResponseDocument getRecordsResponseDocument = GetRecordsResponseDocument.Factory.newInstance();
		try {
			getRecordsResponseDocument = ElasticIpRequestClient.doGetRecordsSearch(elasticIPService, itemNumber);
		} catch (RemoteException re) {
			String errMsg = "An error occurred calling Servicenow from query(), please check the log for more detail";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}

		ElasticIpRequest responseMoa = null;
		if (getRecordsResponseDocument != null && getRecordsResponseDocument.getGetRecordsResponse() != null
				&& getRecordsResponseDocument.getGetRecordsResponse().getGetRecordsResultArray().length > 0) {

			List<GetRecordsResult> list = Arrays
					.asList(getRecordsResponseDocument.getGetRecordsResponse().getGetRecordsResultArray()).stream()
					.limit(1).collect(Collectors.toList());

			try {
				responseMoa = this.getObject(ElasticIpRequest.class);
			} catch (EnterpriseConfigurationObjectException ecoe) {
				String errMsg = "An error occurred retrieving an object from AppConfig. The exception is: "
						+ ecoe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, ecoe);
			}

			try {
				if (list.get(0).getSysId() != null && !list.get(0).getSysId().isEmpty()) {
					responseMoa.setSystemId(list.get(0).getSysId());
					logger.info("list.get(0)..getSysId(): " + list.get(0).getSysId());
				}
				if (list.get(0).getURitmNumber() != null && !list.get(0).getURitmNumber().isEmpty()) {
					responseMoa.setRequestItemNumber(list.get(0).getURitmNumber());
					logger.info("list.get(0).getURitmNumber(): " + list.get(0).getURitmNumber());
				}
				if (list.get(0).getUReqState() != null && !list.get(0).getUReqState().isEmpty()) {
					responseMoa.setRequestState(list.get(0).getUReqState());
					logger.info("list.get(0).getUReqState(): " + list.get(0).getUReqState());
				}
				if (list.get(0).getURitmState() != null && !list.get(0).getURitmState().isEmpty()) {
					responseMoa.setRequestItemState(list.get(0).getURitmState());
					logger.info("list.get(0).getURitmState(): " + list.get(0).getURitmState());
				}
				if (list.get(0).getURequestedFor() != null && !list.get(0).getURequestedFor().isEmpty()) {
					responseMoa.setUserNetID(list.get(0).getURequestedFor());
					logger.info("list.get(0).getURequestedFor(): " + list.get(0).getURequestedFor());
				}
				if (list.get(0).getUAwsPublicIp() != null && !list.get(0).getUAwsPublicIp().isEmpty()) {
					responseMoa.setElasticIpAddress(list.get(0).getUAwsPublicIp());
					logger.info("list.get(0).getUAwsPublicIp(): " + list.get(0).getUAwsPublicIp());
				}
				if (list.get(0).getUAwsPrivateIp() != null && !list.get(0).getUAwsPrivateIp().isEmpty()) {
					responseMoa.setPrivateIpAddress(list.get(0).getUAwsPrivateIp());
					logger.info("list.get(0).getUAwsPrivateIp(): " + list.get(0).getUAwsPrivateIp());
				}
				if (list.get(0).getUAwsDomainName() != null && !list.get(0).getUAwsDomainName().isEmpty()) {
					responseMoa.setDomain(list.get(0).getUAwsDomainName());
					logger.info("list.get(0).getUAwsDomainName(): " + list.get(0).getUAwsDomainName());
				}
			} catch (EnterpriseFieldException efe) {
				String errMsg = "An error occurred setting field for the ElasticIP object.  The exception is: "
						+ efe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}
		}
		return responseMoa;
	}

	@Override
	public void create(ElasticIpRequest elasticIpRequest) throws ProviderException {

		logger.info(LOGTAG + "Ready to execute Generate-Request.");
		InsertResponseDocument insertResponseDocument = null;
		String itemNumber = null;
		try {
			insertResponseDocument = ElasticIpRequestClient.doElasticIPCreate(elasticIPService, elasticIpRequest);
			if (insertResponseDocument != null && insertResponseDocument.getInsertResponse() != null) {
				if (insertResponseDocument.getInsertResponse().getStatusMessage() != null
						&& !insertResponseDocument.getInsertResponse().getStatusMessage().isEmpty()) {

					String[] statusMessage = StringUtil
							.splitString(insertResponseDocument.getInsertResponse().getStatusMessage());
					itemNumber = statusMessage[0];
					logger.info("Item number from the generate() " + itemNumber);
				} else if (insertResponseDocument.getInsertResponse().getErrorMessage() != null
						&& !insertResponseDocument.getInsertResponse().getErrorMessage().isEmpty()) {
					String errMsg = "Error returned from ServiceNow is: "
							+ insertResponseDocument.getInsertResponse().getErrorMessage();
					logger.warn(errMsg);
					throw new ProviderException(errMsg);
				}
			}
		} catch (

		RemoteException re) {
			String errMsg = "An error occurred calling Servicenow from query(), please check the log for more detail";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}
	}

	@Override
	public void delete(String id) throws ProviderException {

		logger.info(LOGTAG + "Ready to execute ElasticIP.Delete-Request.");
		DeleteRecordDocument deleteRecordDocument = DeleteRecordDocument.Factory.newInstance();
		DeleteRecord request = deleteRecordDocument.addNewDeleteRecord();
		request.setSysId(id);
		try {
			 ElasticIpRequestClient.doElasticIPDelete(elasticIPService, id);
		} catch (RemoteException re) {
			String errMsg = "An error occurred calling Servicenow NetIQ from query(), please check the log for more detail";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}		
	}

	@Override
	public void update(ElasticIpRequest elasticIP) throws ProviderException {

		logger.info(LOGTAG + "Ready to execute ElasticIP.Update-Request.");
		//UpdateResponseDocument responseDoc = UpdateResponseDocument.Factory.newInstance();
		try {
			//responseDoc = 
					ElasticIpRequestClient.doElasticIPUpdate(elasticIPService, elasticIP);
		} catch (RemoteException re) {
			String errMsg = "An error occurred calling Servicenow from query(): " + re.getMessage();
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}
	}

	@Override
	public List<XmlEnterpriseObject> query(ElasticIpRequestQuerySpecification querySpec) throws ProviderException {

		logger.info(LOGTAG + "In query(). Ready to execute request.");
		GetRecordsResponseDocument getRecordsResponseDocument = GetRecordsResponseDocument.Factory.newInstance();
		try {
			getRecordsResponseDocument = ElasticIpRequestClient.doGetRecordsSearch(elasticIPService, querySpec);
		} catch (RemoteException re) {
			String errMsg = "An error occurred calling ServiceNow from query(), please check the log for more detail";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}
		//getRecordsResponseDocument.getGetRecordsResponse().ge
		List<XmlEnterpriseObject> responseMoas = null;
		if (getRecordsResponseDocument != null && getRecordsResponseDocument.getGetRecordsResponse() != null
				&& getRecordsResponseDocument.getGetRecordsResponse().getGetRecordsResultArray().length > 0) {

			GetRecordsResponse resultData = getRecordsResponseDocument.getGetRecordsResponse();
			try {
				responseMoas = getElasticIpRequestResponse(resultData);
			} catch (EnterpriseConfigurationObjectException ecoe) {
				String errMsg = "An error occurred retrieving an object from ServiceNow client during query()";
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, ecoe);
			} catch (EnterpriseFieldException efe) {
				String errMsg = "An error occurred retrieving an object from ServiceNow client during query()";
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, efe);
			}
		}
		return responseMoas;
	}

	private List<XmlEnterpriseObject> getElasticIpRequestResponse(GetRecordsResponse object)
			throws EnterpriseConfigurationObjectException, EnterpriseFieldException, ProviderException {

		List<XmlEnterpriseObject> elasticIpRequestObjects = new ArrayList<XmlEnterpriseObject>();
		for (GetRecordsResult recordsResult : object.getGetRecordsResultArray()) {

			ElasticIpRequest elasticIpObject = this.getObject(ElasticIpRequest.class);
			if (recordsResult.getSysId() != null && !recordsResult.getSysId().isEmpty()) {
				elasticIpObject.setSystemId(recordsResult.getSysId());
				logger.info("recordsResult.getSysId(): " + recordsResult.getSysId());
			}
			if (recordsResult.getURitmNumber() != null && !recordsResult.getURitmNumber().isEmpty()) {
				elasticIpObject.setRequestItemNumber(recordsResult.getURitmNumber());
				logger.info("recordsResult.getURitmNumber(): " + recordsResult.getURitmNumber());
			}
			if (recordsResult.getUReqState() != null && !recordsResult.getUReqState().isEmpty()) {
				elasticIpObject.setRequestState(recordsResult.getUReqState());
				logger.info("recordsResult.getUReqState(): " + recordsResult.getUReqState());
			}
			if (recordsResult.getURitmState() != null && !recordsResult.getURitmState().isEmpty()) {
				elasticIpObject.setRequestItemState(recordsResult.getURitmState());
				logger.info("recordsResult.getURitmState(): " + recordsResult.getURitmState());
			}
			if (recordsResult.getUAwsPublicIp() != null && !recordsResult.getUAwsPublicIp().isEmpty()) {
				elasticIpObject.setElasticIpAddress(recordsResult.getUAwsPublicIp());
				logger.info("recordsResult.getUAwsPublicIp(): " + recordsResult.getUAwsPublicIp());
			}
			if (recordsResult.getUAwsPrivateIp() != null && !recordsResult.getUAwsPrivateIp().isEmpty()) {
				elasticIpObject.setPrivateIpAddress(recordsResult.getUAwsPrivateIp());
				logger.info("recordsResult.getUAwsPrivateIp(): " + recordsResult.getUAwsPrivateIp());
			}
			if (recordsResult.getUAwsDomainName() != null && !recordsResult.getUAwsDomainName().isEmpty()) {
				elasticIpObject.setDomain(recordsResult.getUAwsDomainName());
				logger.info("recordsResult.getUAwsDomainName(): " + recordsResult.getUAwsDomainName());
			}
			if (recordsResult.getURequestedFor() != null && !recordsResult.getURequestedFor().isEmpty()) {
				elasticIpObject.setUserNetID(recordsResult.getURequestedFor());
				logger.info("recordsResult.getURequestedFor(): " + recordsResult.getURequestedFor());
			}

			elasticIpRequestObjects.add(elasticIpObject);
		}
		return elasticIpRequestObjects;
	}
	/**
	 * Converts oEAI object type to XmlEnterpriseObject
	 * 
	 * @param aClass
	 * @return
	 * @throws EnterpriseConfigurationObjectException
	 */
	@SuppressWarnings("unchecked")
	private <T extends XmlEnterpriseObjectImpl> T getObject(Class<T> aClass)
			throws EnterpriseConfigurationObjectException {
		return (T) getAppConfig().getObjectByType(aClass.getName());
	}

	/**
	 * Creates a ElasticIP Service stub for the remote object acts as a client's
	 * local proxy, so the caller can invoke a method on the client that will
	 * carry out the method call for the remote object. A multithreaded client
	 * will be set up in order to enable concurrent access.
	 * 
	 * @param userid
	 * @param password
	 * @param endpoint
	 * @param timeout
	 * @return
	 * @throws RemoteException
	 */
	private ServiceNow_u_esb_elastic_ip_request_crudStub getElasticIPStub(String userid, String password,
			String endpoint, int timeout) throws RemoteException {

		ServiceNow_u_esb_elastic_ip_request_crudStub service = null;
		EndpointReference targetEPR = new EndpointReference(endpoint);
		service = new ServiceNow_u_esb_elastic_ip_request_crudStub();
		HttpTransportPropertiesImpl.Authenticator auth = new HttpTransportPropertiesImpl.Authenticator();
		auth.setUsername(userid);
		auth.setPassword(password);
		auth.setPreemptiveAuthentication(true);

		MultiThreadedHttpConnectionManager conmgr = new MultiThreadedHttpConnectionManager();
		Options options = new Options();
		options.setProperty(HTTPConstants.AUTHENTICATE, auth);

		// options.setProperty(HTTPConstants.REUSE_HTTP_CLIENT, "true");
		// Set up concurrent access by creating a pooling manager to manage
		// connections.
		conmgr.getParams().setDefaultMaxConnectionsPerHost(10);
		HttpClient client = new HttpClient(conmgr);
		options.setProperty(HTTPConstants.CACHED_HTTP_CLIENT, client);
		options.setTo(targetEPR);
		options.setTransportInProtocol(Constants.TRANSPORT_HTTP);
		options.setTimeOutInMilliSeconds(timeout);
		service._getServiceClient().setOptions(options);

		return service;
	}

	public AppConfig getAppConfig() {
		return appConfig;
	}

	public void setAppConfig(AppConfig appConfig) {
		this.appConfig = appConfig;
	}

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public String getServiceUserId() {
		return serviceUserId;
	}

	public void setServiceUserId(String serviceUserId) {
		this.serviceUserId = serviceUserId;
	}

	public String getServicePassword() {
		return servicePassword;
	}

	public void setServicePassword(String servicePassword) {
		this.servicePassword = servicePassword;
	}

	public static long getWaitInterval() {
		return waitInterval;
	}

	public static long getMaxWaitTime() {
		return maxWaitTime;
	}

	public String getElasticIPServiceEndpoint() {
		return elasticIPServiceEndpoint;
	}

	public void setElasticIPServiceEndpoint(String elasticIPServiceEndpoint) {
		this.elasticIPServiceEndpoint = elasticIPServiceEndpoint;
	}
}