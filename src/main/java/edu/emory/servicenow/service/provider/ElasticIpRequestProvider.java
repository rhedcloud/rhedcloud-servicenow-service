package edu.emory.servicenow.service.provider;

import java.util.List;
import org.openeai.config.AppConfig;
import org.openeai.moa.XmlEnterpriseObject;
import com.service_now.moa.jmsobjects.customrequests.v1_0.ElasticIpRequest;
import com.service_now.moa.objects.resources.v1_0.ElasticIpRequestQuerySpecification;

public abstract interface ElasticIpRequestProvider {

	void init(AppConfig appConfig) throws ProviderException;
	void create(ElasticIpRequest elasticIpRequest) throws ProviderException;
	List<XmlEnterpriseObject> query(ElasticIpRequestQuerySpecification elasticIP) throws ProviderException;
	ElasticIpRequest query(ElasticIpRequest elasticIP) throws ProviderException;
	void delete(String id) throws ProviderException;
	void update(ElasticIpRequest elasticIP) throws ProviderException;
}
