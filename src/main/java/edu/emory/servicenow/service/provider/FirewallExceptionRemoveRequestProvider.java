package edu.emory.servicenow.service.provider;

import java.util.List;

import org.openeai.config.AppConfig;
import org.openeai.moa.XmlEnterpriseObject;

import com.service_now.moa.jmsobjects.customrequests.v1_0.FirewallExceptionRemoveRequest;
import com.service_now.moa.objects.resources.v1_0.FirewallExceptionRemoveRequestQuerySpecification;
import com.service_now.moa.objects.resources.v1_0.FirewallExceptionRemoveRequestRequisition;

public abstract interface FirewallExceptionRemoveRequestProvider {
	void init(AppConfig appConfig) throws ProviderException;
	List<XmlEnterpriseObject> query(FirewallExceptionRemoveRequestQuerySpecification queryData) throws ProviderException;
	FirewallExceptionRemoveRequest query(String ritmnumber) throws ProviderException;
	void delete(FirewallExceptionRemoveRequest firewallException) throws ProviderException;
	FirewallExceptionRemoveRequest generate(FirewallExceptionRemoveRequestRequisition firewallException) throws ProviderException;
}
