package edu.emory.servicenow.service.provider;

import java.util.List;

import org.openeai.config.AppConfig;
import org.openeai.moa.XmlEnterpriseObject;

import com.service_now.moa.jmsobjects.customrequests.v1_0.FirewallExceptionAddRequest;
import com.service_now.moa.objects.resources.v1_0.FirewallExceptionAddRequestQuerySpecification;
import com.service_now.moa.objects.resources.v1_0.FirewallExceptionAddRequestRequisition;

public abstract interface FirewallExceptionAddRequestProvider {

	void init(AppConfig appConfig) throws ProviderException;
	List<XmlEnterpriseObject> query(FirewallExceptionAddRequestQuerySpecification queryData) throws ProviderException;
	FirewallExceptionAddRequest query(String ritmnumber) throws ProviderException;
	void delete(FirewallExceptionAddRequest firewallException) throws ProviderException;
	FirewallExceptionAddRequest generate(FirewallExceptionAddRequestRequisition firewallException) throws ProviderException;
}
