package edu.emory.servicenow.service.client;

//import edu.emory.axis2.servicenow.incident.ServiceNow_incidentStub;
import com.service_now.www.ServiceNow_incidentStub;
import java.math.BigInteger;
import java.rmi.RemoteException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Category;
import org.openeai.OpenEaiObject;

import com.service_now.moa.jmsobjects.servicedesk.v2_0.Incident;
import com.service_now.moa.objects.resources.v2_0.IncidentQuerySpecification;
import com.service_now.moa.objects.resources.v2_0.IncidentRequisition;
import com.service_now.www.incident.DeleteRecordDocument;
import com.service_now.www.incident.DeleteRecordDocument.DeleteRecord;
import com.service_now.www.incident.DeleteRecordResponseDocument;
import com.service_now.www.incident.GetRecordsResponseDocument;
import com.service_now.www.incident.GetRecordsDocument;
import com.service_now.www.incident.GetRecordsDocument.GetRecords;
import com.service_now.www.incident.InsertDocument;
import com.service_now.www.incident.InsertDocument.Insert;
import com.service_now.www.incident.InsertResponseDocument;
import com.service_now.www.incident.UpdateDocument;
import com.service_now.www.incident.UpdateDocument.Update;
import com.service_now.www.incident.UpdateResponseDocument;

/**
 * This web service Soap Client provides wrappers to allow local methods to
 * access ServiceNow web services.
 * 
 * @author RXING2
 */
public class IncidentClient {

	private static Category logger = OpenEaiObject.logger;
	private static final String LOGTAG = "[IncidentClient] ";

	public static DeleteRecordResponseDocument doIncidentDelete(ServiceNow_incidentStub service,
			String itemNumber) throws RemoteException {

		logger.info(LOGTAG + "In doIncidentDelete()");
		DeleteRecordDocument deleteRecordDocument = DeleteRecordDocument.Factory.newInstance();
		DeleteRecord request = deleteRecordDocument.addNewDeleteRecord();
		request.setSysId(itemNumber);

		DeleteRecordResponseDocument responseDoc = DeleteRecordResponseDocument.Factory.newInstance();
		responseDoc = service.deleteRecord(deleteRecordDocument);
		
		return responseDoc;
	}

	public static InsertResponseDocument doIncidentCreate(ServiceNow_incidentStub incidentService,
			IncidentRequisition incidentRequisition) throws RemoteException {
		
		logger.info(LOGTAG + "In doIncidentCreate()");
		InsertDocument insertDocument = InsertDocument.Factory.newInstance();
		Insert request = insertDocument.addNewInsert();
		request.setCorrelationDisplay("ESB Created Incident");
		request.setURecordType("Incident");

		if (incidentRequisition.getCategory() != null && incidentRequisition.getCategory().length() > 0) {
			System.out.println("insert: incidentRequisition.getCategory()" + incidentRequisition.getCategory());
			request.setCategory(incidentRequisition.getCategory());
		}
		if (incidentRequisition.getSubCategory() != null && incidentRequisition.getSubCategory().length() > 0) {
			System.out.println("insert: incidentRequisition.getSubCategory()" + incidentRequisition.getSubCategory());
			request.setSubcategory(incidentRequisition.getSubCategory());
		}
		if (incidentRequisition.getImpact() != null && incidentRequisition.getImpact().length() > 0) {
			request.setImpact(BigInteger.valueOf(Integer.parseInt(incidentRequisition.getImpact())));
		}
		if (incidentRequisition.getUrgency() != null && incidentRequisition.getUrgency().length() > 0) {
			System.out.println("insert: BigInteger.valueOf(Integer.parseInt(incidentRequisition.getUrgency())" + BigInteger.valueOf(Integer.parseInt(incidentRequisition.getUrgency())));
			request.setUrgency(BigInteger.valueOf(Integer.parseInt(incidentRequisition.getUrgency())));
		}
		if (incidentRequisition.getContactType() != null && incidentRequisition.getContactType().length() > 0) {
			System.out.println("insert: incidentRequisition.getContactType()" + incidentRequisition.getContactType());
			request.setContactType(incidentRequisition.getContactType());
		}
		if (incidentRequisition.getBusinessService() != null && incidentRequisition.getBusinessService().length() > 0) {
			System.out.println("insert: incidentRequisition.getBusinessService()" + incidentRequisition.getBusinessService());
			request.setBusinessService(incidentRequisition.getBusinessService());
		}
		if (incidentRequisition.getCmdbCi() != null && incidentRequisition.getCmdbCi().length() > 0) {
			System.out.println("insert: incidentRequisition.getCmdbCi()" + incidentRequisition.getCmdbCi());
			request.setCmdbCi(incidentRequisition.getCmdbCi());
		}
		if (incidentRequisition.getShortDescription() != null
				&& incidentRequisition.getShortDescription().length() > 0) {
			System.out.println("insert: incidentRequisition.getShortDescription()" + incidentRequisition.getShortDescription());
			request.setShortDescription(incidentRequisition.getShortDescription());
		}
		if (incidentRequisition.getDescription() != null && incidentRequisition.getDescription().length() > 0) {
			request.setDescription(incidentRequisition.getDescription());
		}
		if (incidentRequisition.getCallerId() != null && incidentRequisition.getCallerId().length() > 0) {
			request.setCallerId(incidentRequisition.getCallerId());
		}
		if (StringUtils.isNotBlank(incidentRequisition.getAssignmentGroup())) {
			request.setAssignmentGroup(incidentRequisition.getAssignmentGroup());
		}
			
		InsertResponseDocument responseDoc = InsertResponseDocument.Factory.newInstance();
		System.out.println("*************what is insertDocument: "+insertDocument);
		responseDoc = incidentService.insert(insertDocument);
		logger.info(LOGTAG + "Insert response: " + responseDoc.getInsertResponse().getNumber());
		
		return responseDoc;
	}
	
	public static InsertResponseDocument doIncidentCreate(ServiceNow_incidentStub incidentService,
			Incident incident) throws RemoteException {
		
		logger.info(LOGTAG + "In doIncidentCreate()");
		InsertDocument insertDocument = InsertDocument.Factory.newInstance();
		Insert request = insertDocument.addNewInsert();
		request.setCorrelationDisplay("ESB Created Incident");
		request.setCorrelationId("Incident");
		request.setURecordType("Incident");

		if (incident.getCategory() != null && incident.getCategory().length() > 0) {
			System.out.println("insert: incident.getCategory()" + incident.getCategory());
			request.setCategory(incident.getCategory());
		}
		if (incident.getSubCategory() != null && incident.getSubCategory().length() > 0) {
			System.out.println("insert: incident.getSubCategory()" + incident.getSubCategory());
			request.setSubcategory(incident.getSubCategory());
		}
		if (incident.getImpact() != null && incident.getImpact().length() > 0) {
			request.setImpact(BigInteger.valueOf(Integer.parseInt(incident.getImpact())));
		}
		if (incident.getUrgency() != null && incident.getUrgency().length() > 0) {
			System.out.println("insert: BigInteger.valueOf(Integer.parseInt(incident.getUrgency())" + BigInteger.valueOf(Integer.parseInt(incident.getUrgency())));
			request.setUrgency(BigInteger.valueOf(Integer.parseInt(incident.getUrgency())));
		}
		if (incident.getContactType() != null && incident.getContactType().length() > 0) {
			System.out.println("insert: incident.getContactType()" + incident.getContactType());
			request.setContactType(incident.getContactType());
		}
		if (incident.getBusinessService() != null && incident.getBusinessService().length() > 0) {
			System.out.println("insert: incident.getBusinessService()" + incident.getBusinessService());
			request.setBusinessService(incident.getBusinessService());
		}
		if (incident.getCmdbCi() != null && incident.getCmdbCi().length() > 0) {
			System.out.println("insert: incident.getCmdbCi()" + incident.getCmdbCi());
			request.setCmdbCi(incident.getCmdbCi());
		}
		if (incident.getShortDescription() != null
				&& incident.getShortDescription().length() > 0) {
			System.out.println("insert: incident.getShortDescription()" + incident.getShortDescription());
			request.setShortDescription(incident.getShortDescription());
		}
		if (incident.getDescription() != null && incident.getDescription().length() > 0) {
			request.setDescription(incident.getDescription());
		}
		if (incident.getCallerId() != null && incident.getCallerId().length() > 0) {
			request.setCallerId(incident.getCallerId());
		}
		
		InsertResponseDocument responseDoc = InsertResponseDocument.Factory.newInstance();
		//System.out.println("insertDocument: "+insertDocument);
		responseDoc = incidentService.insert(insertDocument);
		logger.info(LOGTAG + "Insert response: " + responseDoc.getInsertResponse().getNumber());
		
		return responseDoc;
	}

	public static GetRecordsResponseDocument doGetRecordsSearch(ServiceNow_incidentStub incidentService, IncidentQuerySpecification queryData)
			throws RemoteException {
		
		logger.info(LOGTAG + "In doGetRecordsSearch()");
		GetRecordsDocument getRecordsDocument = GetRecordsDocument.Factory.newInstance();
		GetRecords getRecords = getRecordsDocument.addNewGetRecords();
		
		if (queryData.getNumber() != null && !queryData.getNumber().isEmpty()){
			getRecords.setNumber(queryData.getNumber());
		}
		if (queryData.getSystemId() != null && !queryData.getSystemId().isEmpty()){
			getRecords.setSysId(queryData.getSystemId());
		}
		if (queryData.getNocAlarmId() != null && !queryData.getNocAlarmId().isEmpty()){
			getRecords.setUNocAlarmId(queryData.getNocAlarmId());
		}
		if (queryData.getCallerId() != null && !queryData.getCallerId().isEmpty()){
			getRecords.setCallerId(queryData.getCallerId());
		}
		if (queryData.getCategory() != null && !queryData.getCategory().isEmpty()){
			getRecords.setCategory(queryData.getCategory());
		}
		if (queryData.getCmdbCi() != null && !queryData.getCmdbCi().isEmpty()){
			getRecords.setCmdbCi(queryData.getCmdbCi());
		}
		if (queryData.getShortDescription() != null && !queryData.getShortDescription().isEmpty()){
			getRecords.setShortDescription(queryData.getShortDescription());
		}
		if (queryData.getSubCategory() != null && !queryData.getSubCategory().isEmpty()){
			getRecords.setSubcategory(queryData.getSubCategory());
		}
		if (queryData.getDescription() != null && !queryData.getDescription().isEmpty()){
			getRecords.setDescription(queryData.getDescription());
		}
		if (queryData.getBusinessService() != null && !queryData.getBusinessService().isEmpty()){
			getRecords.setBusinessService(queryData.getBusinessService());
		}	
		if (queryData.getUrgency() != null && !queryData.getUrgency().isEmpty()){
			getRecords.setUrgency(BigInteger.valueOf(Integer.parseInt(queryData.getUrgency())));
		}		
	
		GetRecordsResponseDocument responseDoc = GetRecordsResponseDocument.Factory.newInstance();
		System.out.println("passing in query document: "+getRecordsDocument.xmlText());
		responseDoc = incidentService.getRecords(getRecordsDocument);
		
		return responseDoc;
	}
	
	public static GetRecordsResponseDocument doGetRecordsSearch(ServiceNow_incidentStub incidentService, String queryData)
			throws RemoteException {
		
		logger.info(LOGTAG + "In doGetRecordsSearch()");
		GetRecordsDocument getRecordsDocument = GetRecordsDocument.Factory.newInstance();
		GetRecords getRecords = getRecordsDocument.addNewGetRecords();
		getRecords.setNumber(queryData);
		GetRecordsResponseDocument responseDoc = GetRecordsResponseDocument.Factory.newInstance();
		System.out.println("passing in query document: "+getRecordsDocument.xmlText());
		responseDoc = incidentService.getRecords(getRecordsDocument);
		
		return responseDoc;
	}

	public static UpdateResponseDocument doIncidentUpdate(ServiceNow_incidentStub incidentService, Incident incident)
			throws RemoteException {
		logger.info(LOGTAG + "In doIncidentCreate()");
		
		UpdateDocument insertDocument = UpdateDocument.Factory.newInstance();
		Update request = insertDocument.addNewUpdate();
		//request.setCorrelationDisplay("ESB Created Request");

		if (incident.getCategory() != null && incident.getCategory().length() > 0) {
			request.setCategory(incident.getCategory());
		}
		if (incident.getSubCategory() != null && incident.getSubCategory().length() > 0) {
			request.setSubcategory(incident.getSubCategory());
		}
		if (incident.getImpact() != null && incident.getImpact().length() > 0) {
			request.setImpact(BigInteger.valueOf(Integer.parseInt(incident.getImpact())));
		}
		if (incident.getUrgency() != null && incident.getUrgency().length() > 0) {
			request.setUrgency(BigInteger.valueOf(Integer.parseInt(incident.getUrgency())));
		}
		if (incident.getContactType() != null && incident.getContactType().length() > 0) {
			request.setContactType(incident.getContactType());
		}
		if (incident.getBusinessService() != null && incident.getBusinessService().length() > 0) {
			request.setBusinessService(incident.getBusinessService());
		}
		if (incident.getCmdbCi() != null && incident.getCmdbCi().length() > 0) {
			request.setCmdbCi(incident.getCmdbCi());
		}
		if (incident.getShortDescription() != null
				&& incident.getShortDescription().length() > 0) {
			request.setShortDescription(incident.getShortDescription());
		}
		if (incident.getDescription() != null && incident.getDescription().length() > 0) {
			request.setDescription(incident.getDescription());
		}
		if (incident.getCallerId() != null && incident.getCallerId().length() > 0) {
			request.setCallerId(incident.getCallerId());
		}
		if (incident.getSystemId() != null && incident.getSystemId().length() > 0) {
			request.setSysId(incident.getSystemId());
		}
		
		UpdateResponseDocument responseDoc = UpdateResponseDocument.Factory.newInstance();
		responseDoc = incidentService.update(insertDocument);
		logger.info(LOGTAG + "Update response: " + responseDoc.getUpdateResponse().getSysId());
		
		return responseDoc;
	}
}
