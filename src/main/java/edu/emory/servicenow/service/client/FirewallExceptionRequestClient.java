package edu.emory.servicenow.service.client;

import java.rmi.RemoteException;

import org.apache.log4j.Category;
import org.apache.log4j.Logger;

import com.service_now.moa.objects.resources.v1_0.FirewallExceptionAddRequestQuerySpecification;
import com.service_now.moa.objects.resources.v1_0.FirewallExceptionAddRequestRequisition;
import com.service_now.moa.objects.resources.v1_0.FirewallExceptionRemoveRequestQuerySpecification;
import com.service_now.moa.objects.resources.v1_0.FirewallExceptionRemoveRequestRequisition;
import com.service_now.www.u_esb_firewall_request_crud.DeleteRecordDocument;
import com.service_now.www.u_esb_firewall_request_crud.DeleteRecordDocument.DeleteRecord;
import com.service_now.www.u_esb_firewall_request_crud.DeleteRecordResponseDocument;
import com.service_now.www.u_esb_firewall_request_crud.GetRecordsDocument;
import com.service_now.www.u_esb_firewall_request_crud.GetRecordsDocument.GetRecords;
import com.service_now.www.u_esb_firewall_request_crud.GetRecordsResponseDocument;
import com.service_now.www.u_esb_firewall_request_crud.InsertDocument;
import com.service_now.www.u_esb_firewall_request_crud.InsertDocument.Insert;
import com.service_now.www.u_esb_firewall_request_crud.InsertResponseDocument;
import com.service_now.www.u_esb_firewall_request_crud.UpdateDocument;
import com.service_now.www.u_esb_firewall_request_crud.UpdateDocument.Update;
import com.service_now.www.u_esb_firewall_request_crud.UpdateResponseDocument;

import edu.emory.axis2.servicenow.firewallexception.ServiceNow_u_esb_firewall_request_crudStub;
import edu.emory.servicenow.service.util.Compliance;
import edu.emory.servicenow.service.util.ServiceNowValues;

/**
 * This web service Soap Client provides wrappers to allow local methods to
 * access ServiceNow web services.
 * 
 * @author RXING2
 * @author nchen3
 */
public class FirewallExceptionRequestClient {

	private static Category logger = Logger.getLogger(FirewallExceptionRequestClient.class);
	private static final String LOGTAG = "[FirewallExceptionRequestClient] ";

	/**
	 * 
	 * @param service
	 * @param queryData
	 * @return
	 * @throws RemoteException
	 * @throws org.apache.axis2.client.NrfServiceExceptionException
	 */
	public static InsertResponseDocument doFirewallExceptionCreate(ServiceNow_u_esb_firewall_request_crudStub service,
			FirewallExceptionAddRequestRequisition createData) throws RemoteException {

		logger.info(LOGTAG + "In doFirewallExceptionCreate(AddRequestRequisition) ");
		InsertDocument insertDocument = InsertDocument.Factory.newInstance();

		Insert request = insertDocument.addNewInsert();

		request.setUCorrelationDisplay(ServiceNowValues.ESB_CREATED_REQUEST.getName());
		request.setUCorrelationId(ServiceNowValues.FIREWALL_REQUEST.getName());
		request.setUNeedFirewall(ServiceNowValues.ADD_EXCEPTION.getName());

		request.setUTechContact(createData.getUserNetID());
		request.setURequestedFor(createData.getUserNetID());
		request.setUCommonName(createData.getApplicationName());
		request.setUInternetAccess(createData.getSourceOutsideEmory());
		request.setUTraverseVpn(createData.getWillTraverseVPN());
		if("yes".equalsIgnoreCase(createData.getWillTraverseVPN())) {
			request.setUVpnName(createData.getVPNName());
		}
		request.setUAmazonVpc(createData.getAccessAwsVPC());
		request.setUHowLong(createData.getTimeRule());
				
		
		String dateString = "";
		if (createData.getValidUntilDate() != null && createData.getValidUntilDate().getYear() != null
				&& createData.getValidUntilDate().getYear().length() > 0
				&& createData.getValidUntilDate().getMonth() != null
				&& createData.getValidUntilDate().getMonth().length() > 0
				&& createData.getValidUntilDate().getDay() != null
				&& createData.getValidUntilDate().getDay().length() > 0) {
			dateString = createData.getValidUntilDate().getYear() + "-" + createData.getValidUntilDate().getMonth()
					+ "-" + createData.getValidUntilDate().getDay();
			request.setUSpecificDate(dateString);
		}
		request.setUIpAddress(createData.getSourceIpAddresses());
		request.setUIpAddress2(createData.getDestinationIpAddresses());
		request.setUPorts(createData.getPorts());
		request.setUBusinessReason(createData.getBusinessReason());
		request.setUPatchServer(createData.getPatched());
		request.setUJustification(createData.getNotPatchedJustification());
		request.setUAppPswd(createData.getDefaultPasswdChanged());
		request.setUJustification1(createData.getPasswdNotChangedJustification());
		request.setUMgmtConsol(createData.getAppConsoleACLed());
		request.setUJustification2(createData.getNotACLedJustification());
		request.setUGuidelines(createData.getHardened());
		request.setUJustification3(createData.getNotHardenedJustification());
		request.setUPlan(createData.getPatchingPlan());

		for (Object object : createData.getCompliance()) {
			if (object.toString().equalsIgnoreCase(Compliance.HIPAA.getName())) {
				request.setUComp1(object.toString());
			}
			if (object.toString().equalsIgnoreCase(Compliance.FERPA.getName())) {
				request.setUComp2(object.toString());
			}
			if (object.toString().equalsIgnoreCase(Compliance.FISMA.getName())) {
				request.setUComp3(object.toString());
			}
			if (object.toString().equalsIgnoreCase(Compliance.OTHER.getName())) {
				request.setUComp4(object.toString());
			}
			if (object.toString().equalsIgnoreCase(Compliance.PCI.getName())) {
				request.setUComp5(object.toString());
			}
			if (object.toString().equalsIgnoreCase(Compliance.UNSURE.getName())) {
				request.setUComp6(object.toString());
			}
		}

		request.setUOtherCompliance(createData.getOtherCompliance());
		request.setUSensitivity(createData.getSensitiveDataDesc());
		request.setUFirewallRules(createData.getLocalFirewallRules());
		request.setUSubnet(createData.getDefaultDenyZone());
		
		logger.info(LOGTAG + "Tags: " + createData.getTag().get(0).toString());

		@SuppressWarnings("unchecked")
		String tags = String.join(",", createData.getTag());
		request.setUTag(tags);

		InsertResponseDocument responseDoc = InsertResponseDocument.Factory.newInstance();
		logger.info(LOGTAG + "Insert document.." + insertDocument.xmlText());
		responseDoc = service.insert(insertDocument);

		logger.info(
				LOGTAG + "Response doFirewallExceptionCreate(AddRequestRequisition): " + responseDoc.getInsertResponse().getStatusMessage());
		return responseDoc;
	}

	/**
	 * 
	 * @param service
	 * @param queryData
	 * @return
	 * @throws RemoteException
	 * @throws org.apache.axis2.client.NrfServiceExceptionException
	 */
	public static InsertResponseDocument doFirewallExceptionCreate(ServiceNow_u_esb_firewall_request_crudStub service,
			FirewallExceptionRemoveRequestRequisition createData) throws RemoteException {
		
		logger.info(LOGTAG + "In doFirewallExceptionCreate(RemoveRequestRequisition) ");
		InsertDocument insertDocument = InsertDocument.Factory.newInstance();

		Insert request = insertDocument.addNewInsert();

		request.setUCorrelationDisplay(ServiceNowValues.ESB_CREATED_REQUEST.getName());
		request.setUCorrelationId(ServiceNowValues.FIREWALL_REQUEST.getName());
		request.setUNeedFirewall(ServiceNowValues.REMOVE_EXCEPTION.getName());

		request.setURequestedFor(createData.getUserNetID());
		request.setUTechContact(createData.getUserNetID());
		request.setURemoveWhat(createData.getRequestDetails());
		
		@SuppressWarnings("unchecked")
		String tags = String.join(",", createData.getTag());
		request.setUTag(tags);
		
		InsertResponseDocument responseDoc = InsertResponseDocument.Factory.newInstance();
		logger.info(LOGTAG + "Insert document.." + insertDocument.xmlText());
		responseDoc = service.insert(insertDocument);

		logger.info(
				LOGTAG + "Response doFirewallExceptionCreate(RemoveRequestRequisition): " + responseDoc.getInsertResponse().getStatusMessage());
		return responseDoc;
	}

	/**
	 * 
	 * @param service
	 * @param itemNumber
	 * @return
	 * @throws RemoteException
	 */
	public static GetRecordsResponseDocument doGetRecordsSearch(ServiceNow_u_esb_firewall_request_crudStub service,
			FirewallExceptionAddRequestQuerySpecification querySpec) throws RemoteException {

		logger.info(LOGTAG + "In doGetRecordsSearch() for AddRequest.  Query Spec is: "+querySpec);
		GetRecordsDocument getRecordsDocument = GetRecordsDocument.Factory.newInstance();
		GetRecords getRecords = getRecordsDocument.addNewGetRecords();
		getRecords.setUCorrelationDisplay(ServiceNowValues.ESB_CREATED_REQUEST.getName());
		getRecords.setUCorrelationId(ServiceNowValues.FIREWALL_REQUEST.getName());
		getRecords.setUNeedFirewall(ServiceNowValues.ADD_EXCEPTION.getName());

		if (querySpec.getRequestItemNumber() != null && querySpec.getRequestItemNumber().length() > 0) {
			getRecords.setURitmNumber(querySpec.getRequestItemNumber());
		}

		if (querySpec.getRequestItemState() != null && querySpec.getRequestItemState().length() > 0) {
			getRecords.setURitmState(querySpec.getRequestItemState());
		}
		
		if (querySpec.getRequestState() != null && querySpec.getRequestState().length() > 0) {
			getRecords.setUReqState(querySpec.getRequestState());
		}
		
		if (querySpec.getSystemId() != null && querySpec.getSystemId().length() > 0) {
			getRecords.setSysId(querySpec.getSystemId());
		}
		
		//if (querySpec.getRemoveItem() != null && querySpec.getRemoveItem().length() > 0) {
		//	getRecords.setURemoveWhat(querySpec.getRemoveItem());
		//}
		//if (querySpec.getNeedFirewall() != null && querySpec.getNeedFirewall().length() > 0) {
		//	getRecords.setUNeedFirewall(querySpec.getNeedFirewall());
		//}

		if (querySpec.getUserNetID() != null && querySpec.getUserNetID().length() > 0) {
			getRecords.setURequestedFor(querySpec.getUserNetID());
		}
		if (querySpec.getApplicationName() != null && querySpec.getApplicationName().length() > 0) {
			getRecords.setUCommonName(querySpec.getApplicationName());
		}
		if (querySpec.getSourceOutsideEmory() != null && querySpec.getSourceOutsideEmory().length() > 0) {
			getRecords.setUInternetAccess(querySpec.getSourceOutsideEmory());
		}
		if (querySpec.getTimeRule() != null && querySpec.getTimeRule().length() > 0) {
			getRecords.setUHowLong(querySpec.getTimeRule());
		}
		if (querySpec.getSourceIpAddresses() != null && querySpec.getSourceIpAddresses().length() > 0) {
			getRecords.setUIpAddress(querySpec.getSourceIpAddresses());
		}
		if (querySpec.getDestinationIpAddresses() != null && querySpec.getDestinationIpAddresses().length() > 0) {
			getRecords.setUIpAddress2(querySpec.getDestinationIpAddresses());
		}
		if (querySpec.getPorts() != null && querySpec.getPorts().length() > 0) {
			getRecords.setUPorts(querySpec.getPorts());
		}
		if (querySpec.getBusinessReason() != null && querySpec.getBusinessReason().length() > 0) {
			getRecords.setUBusinessReason(querySpec.getBusinessReason());
		}
		if (querySpec.getPatched() != null && querySpec.getPatched().length() > 0) {
			getRecords.setUPatchServer(querySpec.getPatched());
		}
		if (querySpec.getDefaultPasswdChanged() != null && querySpec.getDefaultPasswdChanged().length() > 0) {
			getRecords.setUAppPswd(querySpec.getDefaultPasswdChanged());
		}
		if (querySpec.getAppConsoleACLed() != null && querySpec.getAppConsoleACLed().length() > 0) {
			getRecords.setUMgmtConsol(querySpec.getAppConsoleACLed());
		}
		if (querySpec.getHardened() != null && querySpec.getHardened().length() > 0) {
			getRecords.setUGuidelines(querySpec.getHardened());
		}
		if (querySpec.getPatchingPlan() != null && querySpec.getPatchingPlan().length() > 0) {
			getRecords.setUPlan(querySpec.getPatchingPlan());
		}

		String dateString = null;

		if (querySpec.getValidUntilDate() != null && querySpec.getValidUntilDate().getYear() != null
				&& querySpec.getValidUntilDate().getMonth() != null && querySpec.getValidUntilDate().getDay() != null) {
			dateString = querySpec.getValidUntilDate().getYear() + "-" + querySpec.getValidUntilDate().getMonth() + "-"
					+ querySpec.getValidUntilDate().getDay();
			getRecords.setUSpecificDate(dateString);
		}
		
		
		for (Object object : querySpec.getCompliance()) {
			if (object.toString().equals(Compliance.HIPAA.getName())) {
				getRecords.setUComp1(object.toString());
			}
			if (object.toString().equals(Compliance.FERPA.getName())) {
				getRecords.setUComp2(object.toString());
			}
			if (object.toString().equals(Compliance.FISMA.getName())) {
				getRecords.setUComp3(object.toString());
			}
			if (object.toString().equals(Compliance.OTHER.getName())) {
				getRecords.setUComp4(object.toString());
			}
			if (object.toString().equals(Compliance.PCI.getName())) {
				getRecords.setUComp5(object.toString());
			}
			if (object.toString().equals(Compliance.UNSURE.getName())) {
				getRecords.setUComp6(object.toString());
			}
		}
		
		if (querySpec.getTag() != null && querySpec.getTag().size() > 0) {
			for (Object tag : querySpec.getTag()) {
				String enCoded = "u_tagLIKE" + tag;
				getRecords.setEncodedQuery(enCoded);
			}
		}

		if (querySpec.getUserNetID() != null && querySpec.getUserNetID().length() > 0) {
			getRecords.setUTechContact(querySpec.getUserNetID());
		}
		if (querySpec.getOtherCompliance() != null && querySpec.getOtherCompliance().length() > 0) {
			getRecords.setUOtherCompliance(querySpec.getOtherCompliance());
		}
		if (querySpec.getSensitiveDataDesc() != null && querySpec.getSensitiveDataDesc().length() > 0) {
			getRecords.setUSensitivity(querySpec.getSensitiveDataDesc());
		}
		if (querySpec.getLocalFirewallRules() != null && querySpec.getLocalFirewallRules().length() > 0) {
			getRecords.setUFirewallRules(querySpec.getLocalFirewallRules());
		}
		if (querySpec.getDefaultDenyZone() != null && querySpec.getDefaultDenyZone().length() > 0) {
			getRecords.setUSubnet(querySpec.getDefaultDenyZone());
		}

		
		GetRecordsResponseDocument responseDoc = GetRecordsResponseDocument.Factory.newInstance();
		responseDoc = service.getRecords(getRecordsDocument);

		return responseDoc;
	}
	
	/**
	 * 
	 * @param service
	 * @param itemNumber
	 * @return
	 * @throws RemoteException
	 */
	public static GetRecordsResponseDocument doGetRecordsSearch(ServiceNow_u_esb_firewall_request_crudStub service,
			FirewallExceptionRemoveRequestQuerySpecification querySpec) throws RemoteException {
		
		logger.info(LOGTAG + "In doGetRecordsSearch() for RemoveRequest.  Query Spec is: "+querySpec);
		
		GetRecordsDocument getRecordsDocument = GetRecordsDocument.Factory.newInstance();
		GetRecords getRecords = getRecordsDocument.addNewGetRecords();
		getRecords.setUCorrelationDisplay(ServiceNowValues.ESB_CREATED_REQUEST.getName());
		getRecords.setUCorrelationId(ServiceNowValues.FIREWALL_REQUEST.getName());
		getRecords.setUNeedFirewall(ServiceNowValues.REMOVE_EXCEPTION.getName());

		if (querySpec.getRequestItemNumber() != null && querySpec.getRequestItemNumber().length() > 0) {
			getRecords.setURitmNumber(querySpec.getRequestItemNumber());
		}
		
		if (querySpec.getRequestItemState() != null && querySpec.getRequestItemState().length() > 0) {
			getRecords.setURitmState(querySpec.getRequestItemState());
		}
		
		if (querySpec.getRequestState() != null && querySpec.getRequestState().length() > 0) {
			getRecords.setUReqState(querySpec.getRequestState());
		}
		
		if (querySpec.getUserNetID() != null && querySpec.getUserNetID().length() > 0) {
			getRecords.setURequestedFor(querySpec.getUserNetID());
			getRecords.setUTechContact(querySpec.getUserNetID());
		}
		
		if (querySpec.getRequestDetails() != null && querySpec.getRequestDetails().length() > 0) {
			getRecords.setURemoveWhat(querySpec.getRequestDetails());
		}
		
		if (querySpec.getTag() != null && querySpec.getTag().size() > 0) {
			for (Object tag : querySpec.getTag()) {
				String enCoded = "u_tagLIKE" + tag;
				getRecords.setEncodedQuery(enCoded);
			}
		}

		
		GetRecordsResponseDocument responseDoc = GetRecordsResponseDocument.Factory.newInstance();
		responseDoc = service.getRecords(getRecordsDocument);

		return responseDoc;
	}
	
	
	
	/**
	 * 
	 * @param service
	 * @param sysid
	 * @return
	 * @throws RemoteException
	 */
	public static DeleteRecordResponseDocument doFirewallExceptionDelete(
			ServiceNow_u_esb_firewall_request_crudStub service, String sysid) throws RemoteException {

		logger.info(LOGTAG + "In doGetRecordsSearch()");
		DeleteRecordDocument deleteRecordDocument = DeleteRecordDocument.Factory.newInstance();
		DeleteRecord request = deleteRecordDocument.addNewDeleteRecord();
		request.setSysId(sysid);

		DeleteRecordResponseDocument responseDoc = DeleteRecordResponseDocument.Factory.newInstance();
		responseDoc = service.deleteRecord(deleteRecordDocument);
		return responseDoc;
	}


	/**
	 * 
	 * @param firewallExceptionService
	 * @param firewallException
	 * @return
	 * @throws RemoteException
	 */
	public static UpdateResponseDocument doFirewallExceptionUpdate(
			ServiceNow_u_esb_firewall_request_crudStub firewallExceptionService, String sysId, String ritmNumber, String ritmState)
			throws RemoteException {

		logger.info(LOGTAG + "In doFirewallExceptionUpdate()");
		UpdateDocument updateDocument = UpdateDocument.Factory.newInstance();
		Update requestUpdate = updateDocument.addNewUpdate();

		requestUpdate.setUCorrelationDisplay(ServiceNowValues.ESB_CREATED_REQUEST.getName());
		requestUpdate.setUCorrelationId(ServiceNowValues.FIREWALL_REQUEST.getName());

		if (ritmState!= null && ritmState.length() > 0) {
			requestUpdate.setURitmState(ritmState);
		}
		
		if (sysId != null && sysId.length() > 0) {
			requestUpdate.setSysId(sysId);
		}
		
		if (ritmNumber != null && ritmNumber.length() > 0) {
			requestUpdate.setURitmNumber(ritmNumber);
		}

		UpdateResponseDocument responseDoc = UpdateResponseDocument.Factory.newInstance();
		// System.out.println("*********************updatedocument***********************"+updateDocument.xmlText());
		responseDoc = firewallExceptionService.update(updateDocument);
		return responseDoc;
		
	}
}
		
//
//		@SuppressWarnings("unchecked")
//		String tags = String.join(",", updateData.getTag());
//		requestUpdate.setUTag(tags);
//		if (updateData.getTechContact() != null && updateData.getTechContact().length() > 0) {
//			requestUpdate.setUTechContact(updateData.getTechContact());
//		}
//		if (updateData.getOtherCompliance() != null && updateData.getOtherCompliance().length() > 0) {
//			requestUpdate.setUOtherCompliance(updateData.getOtherCompliance());
//		}
//		if (updateData.getSensitiveDataDesc() != null && updateData.getSensitiveDataDesc().length() > 0) {
//			requestUpdate.setUSensitivity(updateData.getSensitiveDataDesc());
//		}
//		if (updateData.getLocalFirewallRules() != null && updateData.getLocalFirewallRules().length() > 0) {
//			requestUpdate.setUFirewallRules(updateData.getLocalFirewallRules());
//		}
//		if (updateData.getDefaultDenyZone() != null && updateData.getDefaultDenyZone().length() > 0) {
//			requestUpdate.setUSubnet(updateData.getDefaultDenyZone());
//		}
//		UpdateResponseDocument responseDoc = UpdateResponseDocument.Factory.newInstance();
//		// System.out.println("*********************updatedocument***********************"+updateDocument.xmlText());
//		responseDoc = firewallExceptionService.update(updateDocument);
//		return responseDoc;
//	}

//	/**
//	 * 
//	 * @param service
//	 * @param itemNumber
//	 * @return
//	 * @throws RemoteException
//	 */
//	public static GetRecordsResponseDocument doGetRecordsSearch(ServiceNow_u_esb_firewall_request_crudStub service,
//			FirewallExceptionRequest queryData) throws RemoteException {
//
//		logger.info(LOGTAG + "In doGetRecordsSearch()");
//		GetRecordsDocument getRecordsDocument = GetRecordsDocument.Factory.newInstance();
//		GetRecords getRecords = getRecordsDocument.addNewGetRecords();
//		getRecords.setUCorrelationDisplay(ServiceNowValues.ESB_CREATED_REQUEST.getName());
//		getRecords.setUCorrelationId(ServiceNowValues.FIREWALL_REQUEST.getName());
//
//		if (queryData.getSystemId() != null && queryData.getSystemId().length() > 0) {
//			getRecords.setSysId(queryData.getSystemId());
//		}
//		GetRecordsResponseDocument responseDoc = GetRecordsResponseDocument.Factory.newInstance();
//		responseDoc = service.getRecords(getRecordsDocument);
//
//		return responseDoc;
//	}
//}
