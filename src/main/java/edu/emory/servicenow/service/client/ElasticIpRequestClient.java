package edu.emory.servicenow.service.client;

import edu.emory.axis2.servicenow.elasticip.ServiceNow_u_esb_elastic_ip_request_crudStub;
import java.rmi.RemoteException;
import org.apache.log4j.Category;
import org.openeai.OpenEaiObject;

import com.service_now.moa.jmsobjects.customrequests.v1_0.ElasticIpRequest;
import com.service_now.moa.objects.resources.v1_0.ElasticIpRequestQuerySpecification;
//import com.service_now.moa.objects.resources.v1_0.ElasticIpRequestRequisition;
import com.service_now.www.u_esb_elastic_ip_request_crud.DeleteRecordDocument;
import com.service_now.www.u_esb_elastic_ip_request_crud.DeleteRecordDocument.DeleteRecord;
import com.service_now.www.u_esb_elastic_ip_request_crud.DeleteRecordResponseDocument;
import com.service_now.www.u_esb_elastic_ip_request_crud.GetRecordsDocument;
import com.service_now.www.u_esb_elastic_ip_request_crud.GetRecordsDocument.GetRecords;
import com.service_now.www.u_esb_elastic_ip_request_crud.GetRecordsResponseDocument;
import com.service_now.www.u_esb_elastic_ip_request_crud.InsertDocument;
import com.service_now.www.u_esb_elastic_ip_request_crud.InsertDocument.Insert;
import com.service_now.www.u_esb_elastic_ip_request_crud.InsertResponseDocument;
import com.service_now.www.u_esb_elastic_ip_request_crud.UpdateDocument;
import com.service_now.www.u_esb_elastic_ip_request_crud.UpdateDocument.Update;
import com.service_now.www.u_esb_elastic_ip_request_crud.UpdateResponseDocument;

/**
 * This web service Soap Client provides wrappers to allow local methods to
 * access ServiceNow web services.
 * 
 * @author RXING2
 */
public class ElasticIpRequestClient {

	private static Category logger = OpenEaiObject.logger;
	private static final String LOGTAG = "[ElasticIpRequestClient] ";

	/**
	 * 
	 * @param service
	 * @param queryData
	 * @return
	 * @throws RemoteException
	 * @throws org.apache.axis2.client.NrfServiceExceptionException
	 */
	public static InsertResponseDocument doElasticIPCreate(ServiceNow_u_esb_elastic_ip_request_crudStub service,
			ElasticIpRequest queryData) throws RemoteException {

		logger.info(LOGTAG + "In doElasticIPCreate()");
		InsertDocument insertDocument = InsertDocument.Factory.newInstance();
		Insert request = insertDocument.addNewInsert();
		request.setUCorrelationDisplay("ESB Created Request");
		request.setUCorrelationId("Elastic IP");
		// request.setUAwsNatType(queryData.getAwsNatType());
		request.setUAwsNatType("new");
		request.setUAwsPublicIp(queryData.getElasticIpAddress());
		request.setUAwsPrivateIp(queryData.getPrivateIpAddress());
		request.setUAwsDomainName(queryData.getDomain());
		request.setURequestedFor(queryData.getUserNetID());

		InsertResponseDocument responseDoc = InsertResponseDocument.Factory.newInstance();
		responseDoc = service.insert(insertDocument);

		logger.info(LOGTAG + "Insert response: " + responseDoc.getInsertResponse().getStatusMessage());
		return responseDoc;
	}

	/**
	 * 
	 * @param service
	 * @param itemNumber
	 * @return
	 * @throws RemoteException
	 */
	public static GetRecordsResponseDocument doGetRecordsSearch(ServiceNow_u_esb_elastic_ip_request_crudStub service,
			ElasticIpRequest elasticIpRequest) throws RemoteException {

		logger.info(LOGTAG + "In doGetRecordsSearch()");
		GetRecordsDocument getRecordsDocument = GetRecordsDocument.Factory.newInstance();
		GetRecords getRecords = getRecordsDocument.addNewGetRecords();
		getRecords.setURitmNumber(elasticIpRequest.getRequestItemNumber());
		GetRecordsResponseDocument responseDoc = GetRecordsResponseDocument.Factory.newInstance();
		responseDoc = service.getRecords(getRecordsDocument);
		return responseDoc;
	}

	public static GetRecordsResponseDocument doGetRecordsSearch(ServiceNow_u_esb_elastic_ip_request_crudStub service,
			ElasticIpRequestQuerySpecification elasticIpRequest) throws RemoteException {

		logger.info(LOGTAG + "In doGetRecordsSearch()");
		GetRecordsDocument getRecordsDocument = GetRecordsDocument.Factory.newInstance();
		GetRecords getRecords = getRecordsDocument.addNewGetRecords();

		if (elasticIpRequest.getRequestState() != null && !elasticIpRequest.getRequestState().isEmpty()) {
			getRecords.setUReqState(elasticIpRequest.getRequestState());
		}
		if (elasticIpRequest.getRequestItemState() != null && !elasticIpRequest.getRequestItemState().isEmpty()) {
			getRecords.setURitmState(elasticIpRequest.getRequestItemState());
		}
		if (elasticIpRequest.getSystemId() != null && !elasticIpRequest.getSystemId().isEmpty()) {
			getRecords.setSysId(elasticIpRequest.getSystemId());
		}
		if (elasticIpRequest.getElasticIpAddress() != null && !elasticIpRequest.getElasticIpAddress().isEmpty()) {
			getRecords.setUAwsPublicIp(elasticIpRequest.getElasticIpAddress());
		}
		if (elasticIpRequest.getPrivateIpAddress() != null && !elasticIpRequest.getPrivateIpAddress().isEmpty()) {
			getRecords.setUAwsPrivateIp(elasticIpRequest.getPrivateIpAddress());
		}
		//if (elasticIpRequest.getAwsNatType() != null && !elasticIpRequest.getAwsNatType().isEmpty()) {
			//System.out.println("getting the nat type: "+elasticIpRequest.getAwsNatType());
			//getRecords.setUAwsNatType(elasticIpRequest.getAwsNatType());
		//}
		if (elasticIpRequest.getDomain() != null && !elasticIpRequest.getDomain().isEmpty()) {
			getRecords.setUAwsDomainName(elasticIpRequest.getDomain());
		}
		if (elasticIpRequest.getUserNetID() != null && !elasticIpRequest.getUserNetID().isEmpty()) {
			getRecords.setURequestedFor(elasticIpRequest.getUserNetID());
		}
		if (elasticIpRequest.getRequestItemNumber() != null && !elasticIpRequest.getRequestItemNumber().isEmpty()) {
			getRecords.setURitmNumber(elasticIpRequest.getRequestItemNumber());
		}

		GetRecordsResponseDocument responseDoc = GetRecordsResponseDocument.Factory.newInstance();
		responseDoc = service.getRecords(getRecordsDocument);
		return responseDoc;
	}

	/**
	 * 
	 * @param service
	 * @param itemNumber
	 * @return
	 * @throws RemoteException
	 */
	public static DeleteRecordResponseDocument doElasticIPDelete(ServiceNow_u_esb_elastic_ip_request_crudStub service,
			String itemNumber) throws RemoteException {

		logger.info(LOGTAG + "In doGetRecordsSearch()");
		DeleteRecordDocument deleteRecordDocument = DeleteRecordDocument.Factory.newInstance();
		DeleteRecord request = deleteRecordDocument.addNewDeleteRecord();
		request.setSysId(itemNumber);

		DeleteRecordResponseDocument responseDoc = DeleteRecordResponseDocument.Factory.newInstance();
		responseDoc = service.deleteRecord(deleteRecordDocument);
		return responseDoc;
	}

	/**
	 * 
	 * @param elasticIPService
	 * @param elasticIP
	 * @return
	 * @throws RemoteException
	 */
	public static UpdateResponseDocument doElasticIPUpdate(
			ServiceNow_u_esb_elastic_ip_request_crudStub elasticIPService, ElasticIpRequest elasticIp)
			throws RemoteException {

		logger.info(LOGTAG + "In doElasticIPUpdate()");
		UpdateDocument updateDocument = UpdateDocument.Factory.newInstance();
		Update request = updateDocument.addNewUpdate();

		if (elasticIp.getDomain() != null && !elasticIp.getDomain().isEmpty()) {
			request.setUAwsDomainName(elasticIp.getDomain());
		}
		if (elasticIp.getSystemId() != null && !elasticIp.getSystemId().isEmpty()) {
			request.setSysId(elasticIp.getSystemId());
		}
		if (elasticIp.getRequestItemNumber() != null && !elasticIp.getRequestItemNumber().isEmpty()) {
			request.setURitmNumber(elasticIp.getRequestItemNumber());
		}
		//if (elasticIp.getAwsNatType() != null && !elasticIp.getAwsNatType().isEmpty()) {
			request.setUAwsNatType("modify");
		//}
		if (elasticIp.getPrivateIpAddress() != null && !elasticIp.getPrivateIpAddress().isEmpty()) {
			request.setUAwsPrivateIp(elasticIp.getPrivateIpAddress());
		}
		if (elasticIp.getElasticIpAddress() != null && !elasticIp.getElasticIpAddress().isEmpty()) {
			request.setUAwsPublicIp(elasticIp.getElasticIpAddress());
		}
		if (elasticIp.getUserNetID() != null && !elasticIp.getUserNetID().isEmpty()) {
			request.setURequestedFor(elasticIp.getUserNetID());
		}

		UpdateResponseDocument responseDoc = UpdateResponseDocument.Factory.newInstance();
		responseDoc = elasticIPService.update(updateDocument);
		return responseDoc;
	}
}
